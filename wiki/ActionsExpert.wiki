#summary All about Actions

In Grove `Actions` is an interface made available for Applications and Modules in order to share actions with the external world.