<?php

namespace welcome;

class HelloProfile extends \mg\Profile {
	
}

interface HelloActions extends \mg\Actions {
	
	public function say(\mg\get $message);
}

class HelloApplication extends \mg\Application implements HelloActions {

	public $debug = null;

	public $message = null;

	public function inject() {
		$this->debug = new \mg\PropertyInjection();
	}

	public function say(\mg\get $message) {
		$this->message = $message();
	}

}
