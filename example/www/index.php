<?php

error_reporting(-1);

include(__DIR__ . "/../mangrove/core/Runtime.php");

// Include the minimal configuration for a normal web application (Grove + Groove)
include(__DIR__ . '/../include/WebConfiguration.php');

// Include the application specific configuration
include(__DIR__ . '/../include/FrameworkConfiguration.php');


use \mg\Runtime, \mg\InternalClassSource, \mg\FileSystemClassSource;

// Configure the Runtime from this entry point (index.php) with the WebConfiguration
$configuration = new WebConfiguration();

$configuration->setDictionaryPath(__DIR__ . '/../assets/language');
$configuration->setTemplatePath(__DIR__ . '/../assets/template');
$configuration->setConfigurationPath(__DIR__ . '/../assets/config');

// Wrap the WebConfiguration in a more approachable custom configuration
$configuration = new FrameworkConfiguration($configuration);

// Create a new Runtime, rooted at ../mg~
$runtime = new Runtime(__DIR__ . '/../mg~');

// Let the Runtime use the FrameworkConfiguration
$runtime->setRuntimeConfiguration($configuration);

// Add the ClassSources which provide the classes required in the Runtime
$runtime->addClassSource(new InternalClassSource());

$runtime->addClassSource(new FileSystemClassSource(__DIR__ . '/../mangrove'));
$runtime->addClassSource(new FileSystemClassSource(__DIR__ . '/../src'));

// Start the framework
$runtime(function(mg\Grove $grove, mg\HttpRequest $request, mg\HttpResponse $response) {
	$grove->setDefaultApplication('welcome');
	$grove->dispatch($request, $response);
});

