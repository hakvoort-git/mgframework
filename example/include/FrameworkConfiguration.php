<?php

/**
 * This file can be used to add application specific bindings
 *
 * @author Michiel Hakvoort
 */

class FrameworkConfiguration implements mg\ContextConfiguration {

	private $parentConfiguration = null;

	public function __construct(mg\ContextConfiguration $parentConfiguration) {
		$this->parentConfiguration = $parentConfiguration;
	}

	public function configure(mg\Binder $binder) {
		if($this->parentConfiguration !== null) {
			$this->parentConfiguration->configure($binder);
		}


	}
}
