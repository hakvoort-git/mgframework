<?php

/**
 * @copyright Michiel Hakvoort 2009
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @filesource
 */

/*
 * Copyright (c) 2009 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

class CliConfiguration implements mg\ContextConfiguration {

	private $dictionaryPath = null;
	private $templatePath = null;
	private $configurationPath = null;
	private $applicationNamespace = null;
	private $moduleNamespace = null;

	public function __construct() {

	}

	public function setDictionaryPath($dictionaryPath) {
		$this->dictionaryPath = $dictionaryPath;
	}

	public function setConfigurationPath($configurationPath) {
		$this->configurationPath = $configurationPath;
	}


	public function configure(mg\Binder $binder) {
		// Globalization
		$binder->bind('mg\G11N')->to('mg\G11N')->in(mg\Scope :: SINGLETON);

		$dictionaryPath = $this->dictionaryPath;

		if($dictionaryPath === null) {
			throw new \mg\Exception('Dictionary path not set');
		}

		// Configured dictionary
		$binder->bind('mg\Dictionary')->to(function() use($dictionaryPath) {
			$resources = array();
			$resources[] = new mg\FileSystemDictionarySource($dictionaryPath);

			return new mg\DefaultDictionary($resources);

		})->in(mg\Scope :: UNLAZY_SINGLETON);

		$binder->bind('mg\InjectableContainerProvider')->to('mg\DefaultInjectableContainerProvider')->in(mg\Scope :: UNLAZY_SINGLETON);
		$binder->bind('mg\InjectionBinder')->to('mg\DefaultInjectionBinder')->in(mg\Scope :: UNLAZY_SINGLETON);
		$binder->bind('mg\InjectableProvider')->to('mg\DefaultInjectableProvider')->in(mg\Scope :: UNLAZY_SINGLETON);

		// Routing implementation
		$binder->bind('mg\Router')->to('mg\DefaultRouter')->in(mg\Scope :: SINGLETON);

		$configurationPath = $this->configurationPath;

		if($configurationPath === null) {
			throw new \mg\Exception('Configuration path not set');
		}

		// Unlazy context dependent binding for the ResourceBundle
		$binder->bind('mg\Properties')->to(function() use($configurationPath) {
			$sources = array();
			$sources[] = new mg\FileSystemPropertySource($configurationPath);

			return new mg\Properties($sources);
		})->in(mg\Scope :: UNLAZY_SINGLETON);


		$binder->bind('mg\FilterProvider')->to('mg\DefaultFilterProvider')->in(mg\Scope :: UNLAZY_SINGLETON);

		$binder->bind('mg\Route')->to(function() {
			return new mg\DefaultRoute(null, null, null, null, null, array());
		});

		// Non-facade request
		$binder->bind('mg\HttpRequest')->to(function(mg\Properties $properties) {
			$httpRequest = new mg\SimulatedHttpRequest();
			$httpRequest->domain = $properties['cli.request.domain'] ?: 'localhost';
			$httpRequest->scriptName = $properties['cli.request.scriptname'] ?: '/index.php';
			$httpRequest->pathInfo = $properties['cli.request.pathinfo'] ?: '';
			$httpRequest->isRewritten = $properties['cli.request.isrewritten'] ?: false;
			$httpRequest->port = intval($properties['cli.request.port']) ?: 80;

			return $httpRequest;
		})->in(mg\Scope :: SINGLETON);

	}

}

