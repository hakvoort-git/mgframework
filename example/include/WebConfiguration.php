<?php

/**
 * @copyright Michiel Hakvoort 2009
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @filesource
 */

/*
 * Copyright (c) 2009 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

class WebConfiguration implements mg\ContextConfiguration {

	private $dictionaryPath = null;
	private $templatePath = null;
	private $configurationPath = null;

	public function __construct() {

	}

	public function setDictionaryPath($dictionaryPath) {
		$this->dictionaryPath = $dictionaryPath;
	}

	public function setTemplatePath($templatePath) {
		$this->templatePath = $templatePath;
	}

	public function setConfigurationPath($configurationPath) {
		$this->configurationPath = $configurationPath;
	}

	public function configure(mg\Binder $binder) {
		// Globalization
		$binder->bind('mg\G11N')->to('mg\G11N')->in(mg\Scope :: SINGLETON);

		// Default for cookies
		$binder->bind('mg\CookieManager')->to('mg\DefaultCookieManager')->in(mg\Scope :: SINGLETON);

		// Default for session
		$binder->bind('mg\SessionManager')->to('mg\DefaultSessionManager')->in(mg\Scope :: SINGLETON);

		// Default resolution for session
		$binder->bind('mg\Session')->to(function(mg\SessionManager $sessionManager) {
			return $sessionManager->openSession();
		})->in(mg\Scope :: SINGLETON);

		$dictionaryPath = $this->dictionaryPath;

		if($dictionaryPath === null) {
			throw new \mg\Exception('Dictionary path not set');
		}

		// Configured dictionary
		$binder->bind('mg\Dictionary')->to(function() use($dictionaryPath) {
			$resources = array();
			$resources[] = new mg\FileSystemDictionarySource($dictionaryPath);

			return new mg\DefaultDictionary($resources);

		})->in(mg\Scope :: UNLAZY_SINGLETON);

		$binder->bind('mg\InjectableContainerProvider')->to('mg\DefaultInjectableContainerProvider')->in(mg\Scope :: UNLAZY_SINGLETON);
		$binder->bind('mg\InjectionBinder')->to('mg\DefaultInjectionBinder')->in(mg\Scope :: UNLAZY_SINGLETON);
		$binder->bind('mg\InjectableProvider')->to('mg\DefaultInjectableProvider')->in(mg\Scope :: UNLAZY_SINGLETON);

		// Framework
		$binder->bind('mg\Grove')->to('mg\Grove')->in(mg\Scope :: SINGLETON);

		$templatePath = $this->templatePath;

		if($templatePath === null) {
			throw new \mg\Exception('Template path not set');
		}

		// Unlazy initialization of Groove
		$binder->bind('mg\Groove')->to(function() use($templatePath) {
			return new mg\Groove($templatePath);
		})->in(mg\Scope :: UNLAZY_SINGLETON);

		// Bind Groove as a ViewProvider for Grove
		$binder->bind('mg\ViewProvider')->to('mg\Groove');

		// Routing implementation
		$binder->bind('mg\Router')->to('mg\DefaultRouter')->in(mg\Scope :: SINGLETON);

		$binder->bind('mg\FilterProvider')->to('mg\DefaultFilterProvider')->in(mg\Scope :: UNLAZY_SINGLETON);

		$configurationPath = $this->configurationPath;

		if($configurationPath === null) {
			throw new \mg\Exception('Configuration path not set');
		}

		// Unlazy context dependent binding for the ResourceBundle
		$binder->bind('mg\Properties')->to(function() use($configurationPath) {
			$sources = array();
			$sources[] = new mg\FileSystemPropertySource($configurationPath);

			return new mg\Properties($sources);
		})->in(mg\Scope :: UNLAZY_SINGLETON);


		$binder->bind('mg\Feedback')->to('mg\Feedback')->in(mg\Scope :: SINGLETON);

		// Non-facade request
		$binder->bind('mg\HttpRequest')->to('mg\DefaultHttpRequest')->in(mg\Scope :: SINGLETON);

		// Non-facade response
		$binder->bind('mg\HttpResponse')->to('mg\DefaultHttpResponse')->in(mg\Scope :: SINGLETON);

		// Application and module creation registries
		$binder->bind('mg\ApplicationProvider')->to('mg\GroveProvider')->in(mg\Scope :: SINGLETON);
		$binder->bind('mg\ModuleProvider')->to('mg\GroveProvider')->in(mg\Scope :: SINGLETON);
		$binder->bind('mg\GroveProvider')->to('mg\GroveProvider')->in(mg\Scope :: SINGLETON);


		// Mapper for module and application names
		$binder->bind('mg\ProfileMapper')->to('mg\NamespaceBasedProfileMapper')->in(mg\Scope :: UNLAZY_SINGLETON);
		$binder->bind('mg\ApplicationMapper')->to('mg\NamespaceBasedApplicationMapper')->in(mg\Scope :: UNLAZY_SINGLETON);
		$binder->bind('mg\ModuleMapper')->to('mg\NamespaceBasedModuleMapper')->in(mg\Scope :: UNLAZY_SINGLETON);

	}

}

