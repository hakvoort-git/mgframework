<?php

namespace mg;

class DatabaseException extends Exception {

    private $sqlStateCode = null;

    public function __construct($message, $errorCode, $sqlStateCode) {
        parent :: __construct($message, $errorCode);

        $this->sqlStateCode = $sqlStateCode;
    }

    /**
     * Get the SQL state code for the occured exception
     *
     * @return int
     */
    public function getSQLState() {
        return $this->sqlStateCode;
    }

}