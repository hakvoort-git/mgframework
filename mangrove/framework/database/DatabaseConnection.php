<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage grove
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

namespace mg;

use PDO;

interface DatabaseConnection {

    public function begin();
    public function commit();
    public function rollBack();

    /**
     *
     * @param string $string
     * @return string
     */
    public function quote($string);

    /**
     * @return \mg\Statement
     */
    public function prepare($statement);
}

class PDODatabaseConnection implements DatabaseConnection {

    /**
     * @var \PDO
     */
    private $pdo = null;

    private $location = null;
    private $username = null;
    private $password = null;

    public function __construct($location, $username = null, $password = null) {
        $this->location = $location;
        $this->username = $username;
        $this->password = $password;
    }


    /**
     * Enter description here...
     *
     */
    public function begin() {
        $this->openConnection();
        if(!$this->pdo->beginTransaction()) {
            $info = $this->pdo->errorInfo();
            throw new DatabaseException($info[2], $info[1], $info[0]);
        }
    }

    /**
     * Enter description here...
     *
     */
    public function commit() {
        $this->openConnection();
        if(!$this->pdo->commit()) {
            $info = $this->pdo->errorInfo();
            throw new DatabaseException($info[2], $info[1], $info[0]);
        }
    }

    /**
     * Enter description here...
     *
     */
    public function rollBack() {
        $this->openConnection();
        if(!$this->pdo->rollBack()) {
            $info = $this->pdo->errorInfo();
            throw new DatabaseException($info[2], $info[1], $info[0]);
        }
    }

    /**
     * Enter description here...
     *
     * @param string $statement
     * @return \mg\Statement
     */
    public function prepare($statement) {
        $this->openConnection();
        return new PDOStatement($this->pdo->prepare($statement), $this->pdo);
    }

    /**
     * Enter description here...
     *
     * @param string $string
     * @return string
     */
    public function quote($string) {
        $this->openConnection();
        return $this->pdo->quote($string);
    }

    /**
     * Open the connection, if the connection is already opened, noop
     */
    private function openConnection() {
        if($this->pdo === null) {
            $this->pdo = new PDO($this->location, $this->username, $this->password, array(PDO::ATTR_PERSISTENT => true));
        }
    }
}
