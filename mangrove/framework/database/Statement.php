<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage grove
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

namespace mg;

use PDO;

interface Statement {

    /**
     * @return \mg\Statement
     */
    public function bindValue($parameter, $value);

    /**
     * @param array $parameters
     *
     * @return \mg\ResultSet
     */
    public function query(array $parameters = null);

    /**
     * @param array $parameters
     *
     * @return int
     */
    public function update(array $parameters = null);

    /**
     * Enter description here...
     *
     * @param array $parameters
     *
     * @return int
     */
    public function insert(array $parameters = null);
}

class PDOStatement implements Statement {

    /**
     *	@var PDOStatement
     */
    private $statement = null;

    /**
     * @var PDO
     */
    private $pdo = null;


    public function __construct(\PDOStatement $statement, PDO $pdo) {
        $this->statement = $statement;
        $this->pdo = $pdo;
    }

    /**
     * Enter description here...
     *
     * @param string $parameter
     * @param mixed $value
     * @return \mg\PDOStatement
     */
    public function bindValue($parameter, $value) {
        if(!$this->statement->bindValue(':' . $parameter, $value)) {
            $info = $this->statement->errorInfo();
            throw new DatabaseException($info[2], $info[1], $info[0]);
        }
        return $this;
    }

    /**
     * Enter description here...
     *
     * @param array $parameters
     * @return \mg\ResultSet
     */
    public function query(array $parameters = null) {
        if($this->statement->execute($parameters)) {
            return new PDOResultSet($this->statement);
        }

        $info = $this->statement->errorInfo();
        throw new DatabaseException($info[2], $info[1], $info[0]);
    }

    /**
     * Enter description here...
     *
     * @param array $parameters
     * @return int
     */
    public function update(array $parameters = null) {
        if($this->statement->execute($parameters)) {
            return $this->statement->rowCount();
        }

        $info = $this->statement->errorInfo();
        throw new DatabaseException($info[2], $info[1], $info[0]);

    }

    /**
     * Enter description here...
     *
     * @param array $parameters
     * @return int
     */
    public function insert(array $parameters = null) {
        if($this->statement->execute($parameters)) {
            return $this->pdo->lastInsertId();
        }

        $info = $this->statement->errorInfo();
        throw new DatabaseException($info[2], $info[1], $info[0]);
    }
}
