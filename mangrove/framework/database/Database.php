<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage grove
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

namespace mg;

use Closure;
use ArrayAccess;

class Database {

    /**
     * Database Connection
     *
     * @var \mg\DatabaseConnection
     */
    private $connection = null;

    public function __construct(DatabaseConnection $connection) {
        $this->connection = $connection;
    }

    /**
     * @return boolean
     */
    public function begin() {
        return $this->connection->begin();
    }

    /**
     * @return boolean
     */
    public function commit() {
        return $this->connection->commit();
    }

    /**
     *
     * @param string $statement
     *
     * @return \mg\DBInsert
     */
    public function insert($statement) {
        $statement = $this->connection->prepare($statement);

        return new DBInsert($statement);
    }

    /**
     *
     * @param string $statement
     *
     * @return \mg\DBUpdate
     */
    public function update($statement) {
        $statement = $this->connection->prepare($statement);

        return new DBUpdate($statement);

    }

    public function delete($statement) {
        return $this->update($statement);
    }

    /**
     * Enter description here...
     *
     * @param string $statement
     * @return \mg\DBQuery
     */
    public function query($statement) {
        $statement = $this->connection->prepare($statement);

        return new DBQuery($statement);
    }

    /**
     *
     * @param \Closure $function
     * @return \mg\DBTransaction
     */
    public function transaction(Closure $closure) {
        return new DBTransaction($this, $closure);
    }

    /**
     * Silently execute a transaction and return the result or the exception
     * as a \mg\DBTransactionResult.
     *
     * @param \Closure $closure
     *
     * @return \mg\DBTransactionResult
     */
    public function __invoke(Closure $closure) {
        try {
            $transaction = $this->transaction($closure);
            return new DBTransactionResult($transaction->execute());
        } catch(\mg\DatabaseException $e) {
            return new DBTransactionResult(null, $e);
        }
    }
    /**
     * Quote a string
     *
     * @param string $string
     * @return string
     */
    public function quote($string) {
        return $this->connection->quote($string);
    }

    /**
     * Enter description here...
     *
     * @param string $statement
     * @return int
     */
    public function executeInsert($statement) {
        $insert = $this->insert($statement);

        if(func_num_args() > 1) {
            $args = func_get_args();
            array_shift($args);
        } else {
            $args = array();
        }

        return call_user_func_array(array($insert, 'execute'), $args);
    }

    /**
     * Enter description here...
     *
     * @param string $statement
     * @return int
     */
    public function executeUpdate($statement) {
        $update = $this->update($statement);

        if(func_num_args() > 1) {
            $args = func_get_args();
            array_shift($args);
        } else {
            $args = array();
        }

        return call_user_func_array(array($update, 'execute'), $args);
    }

    /**
     * Enter description here...
     *
     * @param string $statement
     * @return \mg\ResultSet
     */
    public function executeQuery($statement) {
        $query = $this->query($statement);

        if(func_num_args() > 1) {
            $args = func_get_args();
            array_shift($args);
        } else {
            $args = array();
        }

        return call_user_func_array(array($query, 'execute'), $args);
    }

    /**
     * Enter description here...
     *
     * @return boolean
     */
    public function rollBack() {
        return $this->connection->rollBack();
    }

}

class DBTransaction {

    /**
     * @var \mg\Database
     */
    private $database = null;

    private $closure = null;

    public function __construct(Database $database, Closure $closure) {
        $this->database = $database;
        $this->closure = $closure;
    }

    public function execute() {
        $this->database->begin();
        $result = null;

        try {
            $closure = $this->closure;
            $result = $closure();
            $this->database->commit();

        } catch(\Exception $e) {

            $this->database->rollBack();

            throw $e;
        }

        return $result;
    }
}

class DBTransactionResult {

    protected $payload;
    protected $exception;

    public function __construct($payload, \Exception $exception = null) {
        $this->payload = $payload;
        $this->exception = $exception;
    }

    public function isCommitted() {
        return $this->exception === null;
    }

    public function getPayload() {
        return $this->payload;
    }

    public function getException() {
        return $this->exception;
    }
}

abstract class DBFunction implements ArrayAccess {

    /**
     * @var Statement
     */
    protected $statement = null;

    public function __construct(Statement $statement) {
        $this->statement = $statement;
    }

    /**
     * Enter description here...
     *
     * @param string $key
     * @param string $value
     *
     * @return \mg\DBFunction
     */
    public abstract function set($key, $value);

    public abstract function execute();

    public function offsetExists($offset) {
        return false;
    }

    public function offsetGet($offset) {
        return null;
    }

    public function offsetSet($offset, $value) {
        $this->set($offset, $value);
    }

    public function offsetUnset($offset) {
    }
}

class DBQuery extends DBFunction {

    protected $plan = null;

    public function __construct(Statement $statement) {
        parent :: __construct($statement);

        $this->plan = new DBQueryFetchPlan($this->statement);
    }

    /**
     * Enter description here...
     *
     *  @return \mg\ResultSet
     */
    public function execute() {
        $args = func_get_args();

        if(count($args) === 0) {
            $args = null;
        }

        return $this->plan->execute($args);
    }

    /**
     * Enter description here...
     *
     * @return \mg\DBQuery
     */
    public function rollup($name, $distinct, $rolledUp) {
        if(!is_array($distinct)) {
            $distinct = array($distinct);
        }
        return $this;
    }

    public function getPlan() {
        return $this->plan;
    }

    /**
     * Enter description here...
     *
     * @param int $limit
     * @return \mg\DBQuery
     */
    public function limit($limit) {
        return $this;
    }

    /**
     * Enter description here...
     *
     * @param \mg\DBQuery $query
     * @return \mg\DBQuery
     */
    public function union(DBQuery $query) {
        $this->plan = new DBQueryUnionPlan($this->plan, $query->plan);
        return $this;
    }

    /**
     * Enter description here...
     *
     * @param \mg\DBQuery $query
     * @param mixed $condition
     * @return \mg\DBQuery
     */
    public function merge(DBQuery $query, $condition) {
        return $this;
    }

    /**
     * Enter description here...
     *
     * @param \mg\DBQuery $query
     * @param string $condition
     * @return \mg\DBQuery
     */
    public function join(DBQuery $query, $condition) {
        return $this;
    }

    /**
     * Enter description here...
     *
     * @param \mg\DBQuery $query
     * @param string $condition
     * @return \mg\DBQuery
     */
    public function difference(DBQuery $query, $condition) {
        return $this;
    }


    /**
     * Enter description here...
     *
     * @param string $key
     * @param string $value
     * @return \mg\DBQuery
     */
    public function set($key, $value) {
        $this->statement->bindValue($key, $value);

        return $this;
    }
}

class DBInsert extends DBFunction {

    /**
     * Enter description here...
     *
     * @return int
     */
    public function execute() {
        $args = func_get_args();

        if(count($args) === 0) {
            $args = null;
        }

        return $this->statement->insert($args);
    }

    /**
     * Enter description here...
     *
     * @param string $key
     * @param string $value
     * @return \mg\DBInsert
     */
    public function set($key, $value) {
        $this->statement->bindValue($key, $value);

        return $this;
    }
}

class DBUpdate extends DBFunction {

    /**
     * Enter description here...
     *
     * @return int
     */
    public function execute() {
        $args = func_get_args();

        if(count($args) === 0) {
            $args = null;
        }

        return $this->statement->update($args);
    }

    /**
     * Enter description here...
     *
     * @param string $key
     * @param string $value
     * @return \mg\DBUpdate
     */
    public function set($key, $value) {
        $this->statement->bindValue($key, $value);

        return $this;
    }
}

interface DBQueryPlan {

    /**
     * Enter description here...
     *
     * @return \mg\ResultSet
     */
    public function execute();

}

class DBQueryUnionPlan implements DBQueryPlan {

    private $first = null;
    private $second = null;

    public function __construct(DBQueryPlan $first, DBQueryPlan $second) {
        $this->first = $first;
        $this->second = $second;
    }

    public function execute() {
        return new UnionResultSet($this->first->execute(), $this->second->execute());
    }

}

class DBQueryFetchPlan implements DBQueryPlan {

    private $statement = null;

    public function __construct(Statement $statement) {
        $this->statement = $statement;
    }

    public function execute(array $parameters = null) {
        return $this->statement->query($parameters);
    }
}