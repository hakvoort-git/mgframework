<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage grove
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

namespace mg;

interface ProfileProvider {

    public function getProfile($profileName);

}

interface ApplicationProvider {

    public function getApplication($applicationName);
    public function getApplicationByClass($applicationClass);
    public function getApplicationProfiles($applicationName);

}

interface ModuleProvider {

    /**
     * @return Module
     */
    public function getModule($applicationName, $moduleName);
    public function getModuleByClass($moduleClass);
}

class GroveProvider implements ProfileProvider, ApplicationProvider, ModuleProvider {

    private $profiles = null;
    private $applications = null;
    private $modules = null;

    /**
     * @var ProfileMapper
     */
    private $profileMapper = null;

    /**
     *
     * @var ApplicationMapper
     */
    private $applicationMapper = null;

    /**
     * @var ModuleMapper
     */
    private $moduleMapper = null;

    public function __construct(ProfileMapper $profileMapper, ApplicationMapper $applicationMapper, ModuleMapper $moduleMapper) {
        $this->profileMapper = $profileMapper;
        $this->applicationMapper = $applicationMapper;
        $this->moduleMapper = $moduleMapper;

        $this->profiles = array();
        $this->applications = array();
        $this->modules = array();
    }

    public function getProfile($profileName) {
        if(isset($this->profiles[$profileName])) {
            return $this->profiles[$profileName];
        }

        $className = $this->profileMapper->getProfileClass($profileName);

        if($className === null) {
            return null;
        }

        $instance = get($className);

        $this->profiles[$profileName] = $instance;

        return $instance;
    }

    public function getApplication($applicationName) {
        $applicationName = mb_strtolower($applicationName);

        $className = $this->applicationMapper->getApplicationClass($applicationName);

        if($className === null) {
            return null;
        }

        return $this->getApplicationByClass($className);
    }

    public function getApplicationByClass($className) {
        $className = mb_strtolower($className);

        if(isset($this->applications[$className])) {
            return $this->applications[$className];
        }

        $instance = in(function(InjectableProvider $provider) use ($className) {
            return $provider->createInjectableInstance($className);
        });

        $this->applications[$className] = $instance;

        return $instance;
    }

    /**
     * @return ApplicationPackage
     */
    public function getApplicationProfiles($applicationName) {
        $applicationName = mb_strtolower($applicationName);

        $profiles = $this->applicationMapper->getApplicationProfiles($applicationName);

        $result = array();

        if($profiles === null) {
            return $result;
        }

        foreach($profiles as $profile) {
            $profile = $this->getProfile($profile);
            if($profile !== null) {
                array_unshift($result, $profile);
            }
        }

        return $result;
    }

    public function getModule($applicationName, $moduleName) {
        $applicationName = mb_strtolower($applicationName);
        $moduleName = mb_strtolower($moduleName);

        $className = $this->moduleMapper->getModuleClass($applicationName, $moduleName);

        if($className === null) {
            return null;
        }

        return $this->getModuleByClass($className);
    }

    public function getModuleByClass($className) {
        $className = mb_strtolower($className);

        if(isset($this->modules[$className])) {
            return $this->modules[$className];
        }

        $instance = in(function(InjectableProvider $provider) use ($className) {
            return $provider->createInjectableInstance($className);
        });

        $this->modules[$className] = $instance;

        return $instance;
    }

    public function getModuleController($applicationName, $moduleName) {
        $applicationName = mb_strtolower($applicationName);
        $moduleName = mb_strtolower($moduleName);

        $className = $this->moduleMapper->getModuleControllerClass($applicationName, $moduleName);

        if($className === null) {
            return null;
        }

        return get($className);
    }
}
