<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage grove
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

namespace mg;

use ArrayObject, ArrayAccess, ArrayIterator, IteratorAggregate;

interface Actions {

}

abstract class MixedType {

    public $value = null;

    public function __construct($value) {
        $this->value = $value;
    }

    public function __invoke() {
        return $this->getValue();
    }

    public function getValue() {
        return $this->value;
    }

    public function __toString() {
        return $this->value;
    }

    /**
     * Enter description here...
     *
     * @return int
     */
    public function getInt() {
        $result = filter_var($this->value, FILTER_VALIDATE_INT);

        if($result === false) {
            return null;
        }

        return $result;
    }

    /**
     * Enter description here...
     *
     * @return bool
     */
    public function getBoolean() {
        return filter_var($this->value, FILTER_VALIDATE_BOOLEAN, array('flags' => FILTER_NULL_ON_FAILURE));
    }

    public function getEmailAddress() {
        $result = filter_var($this->value, FILTER_VALIDATE_EMAIL);

        if($result === false) {
            return null;
        }

        return $result;
    }

    public function getUrl() {
        $result = filter_var($this->value, FILTER_VALIDATE_URL);

        if($result === false) {
            return null;
        }

        return $result;
    }

    public function getIpAddress() {
        $result = filter_var($this->value, FILTER_VALIDATE_IP);

        if($result === false) {
            return null;
        }

        return $result;
    }

    /**
     * Enter description here...
     *
     * @return string
     */
    public function getString() {
        return $this->value;
    }

    /**
     * Enter description here...
     *
     * @return float
     */
    public function getFloat() {
        $result = filter_var($this->value, FILTER_VALIDATE_FLOAT, array('flags' => FILTER_FLAG_ALLOW_THOUSAND));

        if($result === false) {
            return null;
        }

        return $result;
    }

}

class get extends MixedType {

    public function __construct($value) {
        parent :: __construct($value);
    }


    public static function resolveInstance($name) {
        $value = in(function(HttpRequest $request) use ($name) {
            return $request->getQueryParameter($name);
        });

        if(!is_string($value)) {
            return null;
        }

        return new get($value);
    }

}

class post extends MixedType {

    public function __construct($value) {
        parent :: __construct($value);
    }

    /**
     *
     * @param $name
     * @return \mg\post
     */
    public static function resolveInstance($name) {
        $value = in(function(HttpRequest $request) use ($name) {
            return $request->getPostParameter($name);
        });

        if(!is_string($value)) {
            return null;
        }

        return new post($value);
    }
}

class postArray extends ArrayObject {

    public static function resolveInstance($name) {
        $value = in(function(HttpRequest $request) use ($name) {
            return $request->getPostParameter($name);
        });

        if(!is_array($value)) {
            return new postArray();
        }

        return new postArray($value);
    }

}

class getArray extends ArrayObject {

    public static function resolveInstance($name) {
        $value = in(function(HttpRequest $request) use ($name) {
            return $request->getQueryParameter($name);
        });

        if(!is_array($value)) {
            return new getArray();
        }

        return new getArray($value);
    }

}

abstract class Data implements ArrayAccess, IteratorAggregate {

    protected $name = null;
    private $data = null;

    public function __construct($name, array $data) {
        $this->name = $name;
        $this->data = $data;
    }

    public function getName() {
        return $this->name;
    }

    public function getIterator() {
        return new ArrayIterator($this->data);
    }

    /**
     * Enter description here...
     *
     * @param string $offset
     * @return int
     */
    public function getInt($name) {
        if(!isset($this[$name])) {
            return null;
        }

        $result = filter_var($this[$name], FILTER_VALIDATE_INT);

        if($result === false) {
            return null;
        }

        return $result;
    }

    /**
     * Enter description here...
     *
     * @param string $offset
     * @return bool
     */
    public function getBoolean($name) {
        if(!isset($this[$name])) {
            return null;
        }

        return filter_var($this[$name], FILTER_VALIDATE_BOOLEAN, array('flags' => FILTER_NULL_ON_FAILURE));
    }

    public function getEmailAddress($name) {
        if(!isset($this[$name])) {
            return null;
        }

        $result = filter_var($this[$name], FILTER_VALIDATE_EMAIL);

        if($result === false) {
            return null;
        }

        return $result;
    }

    public function getUrl($name) {
        if(!isset($this[$name])) {
            return null;
        }

        $result = filter_var($this[$name], FILTER_VALIDATE_URL);

        if($result === false) {
            return null;
        }

        return $result;
    }

    public function getIpAddress($name) {
        if(!isset($this[$name])) {
            return null;
        }

        $result = filter_var($this[$name], FILTER_VALIDATE_IP);

        if($result === false) {
            return null;
        }

        return $result;
    }

    /**
     * Enter description here...
     *
     * @param string $offset
     * @return string
     */
    public function getString($name) {
        if(isset($this[$name]) && is_string($this[$name])) {
            return $this[$name];
        } else {
            return null;
        }
    }

    /**
     * Enter description here...
     *
     * @param string $offset
     * @return array
     */
    public function getArray($name) {
        if(isset($this[$name]) && is_array($this[$name])) {
            return $this[$name];
        } else {
            return null;
        }
    }

    /**
     * Enter description here...
     *
     * @param string $offset
     * @return float
     */
    public function getFloat($name) {
        if(!isset($this[$name])) {
            return null;
        }

        $result = filter_var($this[$name], FILTER_VALIDATE_FLOAT, array('flags' => FILTER_FLAG_ALLOW_THOUSAND));

        if($result === false) {
            return null;
        }

        return $result;
    }

    public function hasField($name) {
        return isset($this[$name]);
    }

    public function offsetExists($offset) {
        return isset($this->data[$offset]);
    }

    public function offsetGet($offset) {
        if(isset($this->data[$offset])) {
            return $this->data[$offset];
        }
        return null;
    }

    public function offsetSet($offset, $value) {

    }

    public function offsetUnset($offset) {

    }

    public abstract function getData($name);
}

class FacadeData extends Data {

    public function getData($name) {
        if(isset($this[$name]) && is_array($this[$name])) {
            return new FacadeData($name, $this[$name]);
        }

        return null;
    }

}

abstract class FormData extends Data {

}



class PostFormData extends FormData {

    public function __construct($name, array $value) {
        parent :: __construct($name, $value);
    }

    public static function resolveInstance($name) {
        $value = in(function(HttpRequest $request) use ($name) {

            $files = $request->getFiles();
            $files = isset($files[$name]) ? $files[$name] : null;

            // Files have a higher precedence than postForms, therefore
            // if there is a single file with the same name as the postForm
            // the postForm cannot exist (and is therefore discarded)
            if($files instanceof UploadedFile) {
                return null;
            }

            $postForm = $request->getPostParameter($name);

            // If there are no files with $name, $postForm doesn't require merging
            if($files === null) {
                return $postForm;
            }

            // As a single string or null cannot be merged with an array of files, the result of the merge are the files
            if(!is_array($postForm)) {
                return $files;
            }

            array_union_recursive($postForm, $files);
            // merge $files and $postForm
            	
            return $postForm;
        });

        if(is_array($value)) {
            return new PostFormData($name, $value);
        }

        return null;
    }

    public function getData($name) {
        if(isset($this[$name]) && is_array($this[$name])) {
            return new PostFormData($name, $this[$name]);
        }

        return null;
    }

}

class GetFormData extends FormData {

    public function __construct($name, array $value) {
        parent :: __construct($name, $value);
    }

    public static function resolveInstance($name) {
        $value = in(function(HttpRequest $request) use ($name) {
            return $request->getQueryParameter($name);
        });

        if(is_array($value)) {
            return new getForm($name, $value);
        }

        return null;
    }

    public function getData($name) {
        if(isset($this[$name]) && is_array($this[$name])) {
            return new GetFormData($name, $this[$name]);
        }

        return null;
    }
}

