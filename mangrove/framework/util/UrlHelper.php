<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage grove
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

namespace mg;

class RouteHelper {

    /**
     * @var HttpRequest
     */
    private $httpRequest = null;


    /**
     * @var Router
     */
    private $router = null;

    public function __construct(HttpRequest $httpRequest, Router $router) {
        $this->httpRequest = $httpRequest;

        $this->router = $router;
    }

    public static function resolveInstance() {
        return in(function(HttpRequest $httpRequest, Router $router) {
            return new RouteHelper($httpRequest, $router);
        });
    }

    /**
     * @return \mg\Url
     */
    public function to($literal = null) {
        return new Url($this->httpRequest, $this->router, $this->router->routeLiteral($literal));
    }

    /**
     * @return \mg\Url
     */
    public function root() {
        return new Url($this->httpRequest, $this->router, Route :: to());
    }

    /**
     * @return \mg\Url
     */
    public function requested() {
        return new Url($this->httpRequest, $this->router, get('mg\externalroute'));
    }

    /**
     * @return \mg\Url
     */
    public function current() {
        return new Url($this->httpRequest, $this->router, get('mg\route'));
    }

    /**
     * @return \mg\Url
     */
    public function p($path = null) {
        return $this->root()->path($path);
    }

    /**
     * @return \mg\Url
     */
    public function path($path = null) {
        return $this->root()->path($path);
    }

    /**
     * @return \mg\Url
     */
    public function a($application = null) {
        return $this->application($application);
    }

    /**
     * @return \mg\Url
     */
    public function application($application = null) {
        return $this->root()->application($application);
    }

    /**
     * @return \mg\Url
     */
    public function m($module = null, $action = null) {
        return $this->module($module, $action);
    }

    /**
     * @return \mg\Url
     */
    public function module($module = null, $action) {
        return $this->root()->module($module, $action);
    }

}

class Url extends MutableRoute implements Viewable, View {

    private $httpRequest = null;
    private $router = null;

    public function __construct(HttpRequest $httpRequest, Router $router, Route $route) {
        parent :: __construct($route->getApplication(), $route->getPath(), $route->getModule(), $route->getAction(), $route->getActionParameters(), $route->isInternal(), $route->isConditional());

        $this->httpRequest = $httpRequest;
        $this->router = $router;
    }

    public function getView() {
        return $this;
    }

    public function write($object, Buffer $targetBuffer) {
        $targetBuffer->write($this->__toString());
    }

    public function toUrl() {
        return $this->router->unroute($this->httpRequest, $this);
    }

    public function __toString() {
        return $this->toUrl();
    }

}
