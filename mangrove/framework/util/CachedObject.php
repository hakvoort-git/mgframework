<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage grove
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

namespace mg;

class CachedObject implements HTTPResponse {

    private $object = null;
    private static $root = null;

    private $file = null;

    public static function __static() {
        self :: $root = mgCore :: getTempFolder('mangrove');
        self :: $root = mgCore :: getTempFolder('mangrove/cache');
    }

    public function __construct($object, $key) {
        $this->object = $object;
        $this->file = self :: $root . DIRECTORY_SEPARATOR . 'c'.$key;
    }

    public function getResponseHeaders() {
        if($this->object instanceof mgHTTPResponse) {
            return $this->object->getResponseHeaders();
        } else {
            return array();
        }
    }

    public function getResponseBody() {
        if(file_exists($this->file)) {
            return file_get_contents($this->file);
        }

        if($this->object instanceof mgHTTPResponse) {
            $result = $this->object->getResponseBody();
        }

        if($result === null) {
            $result = $this->object;
        }

        $view = null;

        if($result instanceof mgViewable) {
            $view = $result->getView();
        }

        $view = $view === null ? mgViewFactory :: getDefaultViewFactory()->createView($result) : $view;

        ob_start();

        $view->view($result);

        $cache = ob_get_clean();

        file_put_contents($this->file, $cache);

        return $cache;
    }

}