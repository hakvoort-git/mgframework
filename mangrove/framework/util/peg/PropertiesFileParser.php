<?php

namespace mg;

use \ParserExpression;

class PropertiesFileParser extends \Parser {

	public $properties = array();

/* File: Line (LT Line)* */
function match_File () {
	$result = $this->construct( "File" );
	$substack = array();
	$_5 = NULL;
	do {
		$key = "Line"; $pos = $this->pos;
		$subres = ( $this->packhas( $key, $pos ) ? $this->packread( $key, $pos ) : $this->packwrite( $key, $pos, $this->match_Line() ) );
		if ($subres !== FALSE) { $this->store( $result, $subres ); }
		else { $_5 = FALSE; break; }
		while (true) {
			$res_4 = $result;
			$pos_4 = $this->pos;
			$_3 = NULL;
			do {
				$key = "LT"; $pos = $this->pos;
				$subres = ( $this->packhas( $key, $pos ) ? $this->packread( $key, $pos ) : $this->packwrite( $key, $pos, $this->match_LT() ) );
				if ($subres !== FALSE) { $this->store( $result, $subres ); }
				else { $_3 = FALSE; break; }
				$key = "Line"; $pos = $this->pos;
				$subres = ( $this->packhas( $key, $pos ) ? $this->packread( $key, $pos ) : $this->packwrite( $key, $pos, $this->match_Line() ) );
				if ($subres !== FALSE) { $this->store( $result, $subres ); }
				else { $_3 = FALSE; break; }
				$_3 = TRUE; break;
			}
			while(0);
			if( $_3 === FALSE) {
				$result = $res_4;
				$this->pos = $pos_4;
				unset( $res_4 );
				unset( $pos_4 );
				break;
			}
		}
		$_5 = TRUE; break;
	}
	while(0);
	if( $_5 === TRUE ) { return $result ; }
	if( $_5 === FALSE) { return FALSE; }
}


/* Line: Property | Comment | EmptyLine */
function match_Line () {
	$result = $this->construct( "Line" );
	$substack = array();
	$_14 = NULL;
	do {
		$res_7 = $result;
		$pos_7 = $this->pos;
		$key = "Property"; $pos = $this->pos;
		$subres = ( $this->packhas( $key, $pos ) ? $this->packread( $key, $pos ) : $this->packwrite( $key, $pos, $this->match_Property() ) );
		if ($subres !== FALSE) {
			$this->store( $result, $subres );
			$_14 = TRUE; break;
		}
		$result = $res_7;
		$this->pos = $pos_7;
		$_12 = NULL;
		do {
			$res_9 = $result;
			$pos_9 = $this->pos;
			$key = "Comment"; $pos = $this->pos;
			$subres = ( $this->packhas( $key, $pos ) ? $this->packread( $key, $pos ) : $this->packwrite( $key, $pos, $this->match_Comment() ) );
			if ($subres !== FALSE) {
				$this->store( $result, $subres );
				$_12 = TRUE; break;
			}
			$result = $res_9;
			$this->pos = $pos_9;
			$key = "EmptyLine"; $pos = $this->pos;
			$subres = ( $this->packhas( $key, $pos ) ? $this->packread( $key, $pos ) : $this->packwrite( $key, $pos, $this->match_EmptyLine() ) );
			if ($subres !== FALSE) {
				$this->store( $result, $subres );
				$_12 = TRUE; break;
			}
			$result = $res_9;
			$this->pos = $pos_9;
			$_12 = FALSE; break;
		}
		while(0);
		if( $_12 === TRUE ) { $_14 = TRUE; break; }
		$result = $res_7;
		$this->pos = $pos_7;
		$_14 = FALSE; break;
	}
	while(0);
	if( $_14 === TRUE ) { return $result ; }
	if( $_14 === FALSE) { return FALSE; }
}

function Line_Property ( &$result, $sub ) { 
        $this->properties[$sub['key']['text']] = trim($sub['value']['text']);
    }

/* EmptyLine: /(\s*)/ */
function match_EmptyLine () {
	$result = array("name"=>"EmptyLine", "text"=>"");
	$substack = array();
	$_16 = new ParserExpression( $this, $substack, $result );
	if (( $subres = $this->rx( $_16->expand('/(\s*)/') ) ) !== FALSE) {
		$result["text"] .= $subres;
		return $result;
	}
	else { return FALSE; }
}


/* Comment: > ( '#' | '!' ) /(.*)/ */
function match_Comment () {
	$result = $this->construct( "Comment" );
	$substack = array();
	$_28 = NULL;
	do {
		if (( $subres = $this->whitespace(  ) ) !== FALSE) { $result["text"] .= $subres; }
		$_24 = NULL;
		do {
			$_22 = NULL;
			do {
				$res_19 = $result;
				$pos_19 = $this->pos;
				if (substr($this->string,$this->pos,1) == '#') {
					$this->pos += 1;
					$result["text"] .= '#';
					$_22 = TRUE; break;
				}
				$result = $res_19;
				$this->pos = $pos_19;
				if (substr($this->string,$this->pos,1) == '!') {
					$this->pos += 1;
					$result["text"] .= '!';
					$_22 = TRUE; break;
				}
				$result = $res_19;
				$this->pos = $pos_19;
				$_22 = FALSE; break;
			}
			while(0);
			if( $_22 === FALSE) { $_24 = FALSE; break; }
			$_24 = TRUE; break;
		}
		while(0);
		if( $_24 === FALSE) { $_28 = FALSE; break; }
		$_26 = new ParserExpression( $this, $substack, $result );
		if (( $subres = $this->rx( $_26->expand('/(.*)/') ) ) !== FALSE) { $result["text"] .= $subres; }
		else { $_28 = FALSE; break; }
		$_28 = TRUE; break;
	}
	while(0);
	if( $_28 === TRUE ) { return $result ; }
	if( $_28 === FALSE) { return FALSE; }
}


/* LT: /\v+/ */
function match_LT () {
	$result = array("name"=>"LT", "text"=>"");
	$substack = array();
	$_30 = new ParserExpression( $this, $substack, $result );
	if (( $subres = $this->rx( $_30->expand('/\v+/') ) ) !== FALSE) {
		$result["text"] .= $subres;
		return $result;
	}
	else { return FALSE; }
}


/* Property: > key:Key > (':' | '=') > value:Value > */
function match_Property () {
	$result = $this->construct( "Property" );
	$substack = array();
	$_45 = NULL;
	do {
		if (( $subres = $this->whitespace(  ) ) !== FALSE) { $result["text"] .= $subres; }
		$key = "Key"; $pos = $this->pos;
		$subres = ( $this->packhas( $key, $pos ) ? $this->packread( $key, $pos ) : $this->packwrite( $key, $pos, $this->match_Key() ) );
		if ($subres !== FALSE) {
			$this->store( $result, $subres, "key" );
		}
		else { $_45 = FALSE; break; }
		if (( $subres = $this->whitespace(  ) ) !== FALSE) { $result["text"] .= $subres; }
		$_40 = NULL;
		do {
			$_38 = NULL;
			do {
				$res_35 = $result;
				$pos_35 = $this->pos;
				if (substr($this->string,$this->pos,1) == ':') {
					$this->pos += 1;
					$result["text"] .= ':';
					$_38 = TRUE; break;
				}
				$result = $res_35;
				$this->pos = $pos_35;
				if (substr($this->string,$this->pos,1) == '=') {
					$this->pos += 1;
					$result["text"] .= '=';
					$_38 = TRUE; break;
				}
				$result = $res_35;
				$this->pos = $pos_35;
				$_38 = FALSE; break;
			}
			while(0);
			if( $_38 === FALSE) { $_40 = FALSE; break; }
			$_40 = TRUE; break;
		}
		while(0);
		if( $_40 === FALSE) { $_45 = FALSE; break; }
		if (( $subres = $this->whitespace(  ) ) !== FALSE) { $result["text"] .= $subres; }
		$key = "Value"; $pos = $this->pos;
		$subres = ( $this->packhas( $key, $pos ) ? $this->packread( $key, $pos ) : $this->packwrite( $key, $pos, $this->match_Value() ) );
		if ($subres !== FALSE) {
			$this->store( $result, $subres, "value" );
		}
		else { $_45 = FALSE; break; }
		if (( $subres = $this->whitespace(  ) ) !== FALSE) { $result["text"] .= $subres; }
		$_45 = TRUE; break;
	}
	while(0);
	if( $_45 === TRUE ) { return $result ; }
	if( $_45 === FALSE) { return FALSE; }
}


/* Key: Symbol ('.' Symbol)* */
function match_Key () {
	$result = $this->construct( "Key" );
	$substack = array();
	$_52 = NULL;
	do {
		$key = "Symbol"; $pos = $this->pos;
		$subres = ( $this->packhas( $key, $pos ) ? $this->packread( $key, $pos ) : $this->packwrite( $key, $pos, $this->match_Symbol() ) );
		if ($subres !== FALSE) { $this->store( $result, $subres ); }
		else { $_52 = FALSE; break; }
		while (true) {
			$res_51 = $result;
			$pos_51 = $this->pos;
			$_50 = NULL;
			do {
				if (substr($this->string,$this->pos,1) == '.') {
					$this->pos += 1;
					$result["text"] .= '.';
				}
				else { $_50 = FALSE; break; }
				$key = "Symbol"; $pos = $this->pos;
				$subres = ( $this->packhas( $key, $pos ) ? $this->packread( $key, $pos ) : $this->packwrite( $key, $pos, $this->match_Symbol() ) );
				if ($subres !== FALSE) { $this->store( $result, $subres ); }
				else { $_50 = FALSE; break; }
				$_50 = TRUE; break;
			}
			while(0);
			if( $_50 === FALSE) {
				$result = $res_51;
				$this->pos = $pos_51;
				unset( $res_51 );
				unset( $pos_51 );
				break;
			}
		}
		$_52 = TRUE; break;
	}
	while(0);
	if( $_52 === TRUE ) { return $result ; }
	if( $_52 === FALSE) { return FALSE; }
}


/* Symbol: /([a-z_][a-z0-9_\[\]]*)/ */
function match_Symbol () {
	$result = array("name"=>"Symbol", "text"=>"");
	$substack = array();
	$_54 = new ParserExpression( $this, $substack, $result );
	if (( $subres = $this->rx( $_54->expand('/([a-z_][a-z0-9_\[\]]*)/') ) ) !== FALSE) {
		$result["text"] .= $subres;
		return $result;
	}
	else { return FALSE; }
}


/* Value: /(.*)/ */
function match_Value () {
	$result = array("name"=>"Value", "text"=>"");
	$substack = array();
	$_56 = new ParserExpression( $this, $substack, $result );
	if (( $subres = $this->rx( $_56->expand('/(.*)/') ) ) !== FALSE) {
		$result["text"] .= $subres;
		return $result;
	}
	else { return FALSE; }
}




}
