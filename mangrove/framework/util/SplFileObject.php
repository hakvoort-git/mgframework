<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage grove
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

namespace mg;

/**
 * A facade interface for the SPL FileObject.
 *
 * @author Michiel Hakvoort
 *
 */
interface SplFileObject extends \SeekableIterator, \RecursiveIterator {

    public function eof();
    public function fgets();

    /**
     * @param delimiter[optional]
     * @param enclosure[optional]
     */
    public function fgetcsv($delimiter = null, $enclosure = null);

    /**
     * @param delimiter[optional]
     * @param enclosure[optional]
     */
    public function setCsvControl($delimiter = null, $enclosure = null);
    public function getCsvControl();

    /**
     * @param operation
     * @param wouldblock[optional]
     */
    public function flock($operation, &$wouldblock = null);
    public function fflush();
    public function ftell();

    /**
     * @param pos
     * @param whence[optional]
     */
    public function fseek($pos, $whence = null);
    public function fgetc();
    public function fpassthru();

    /**
     * @param allowable_tags[optional]
     */
    public function fgetss($allowable_tags = null);

    /**
     * @param format
     */
    public function fscanf($format);

    /**
     * @param str
     * @param length[optional]
     */
    public function fwrite($str, $length = null);
    public function fstat();

    /**
     * @param size
     */
    public function ftruncate($size);

    /**
     * @param flags
     */
    public function setFlags($flags);
    public function getFlags();

    /**
     * @param max_len
     */
    public function setMaxLineLen($max_len);
    public function getMaxLineLen();
    public function getCurrentLine();
    public function __toString();
    public function getPath();
    public function getFilename();

    /**
     * @param suffix[optional]
     */
    public function getBasename($suffix = null);
    public function getPathname();
    public function getPerms();
    public function getInode();
    public function getSize();
    public function getOwner();
    public function getGroup();
    public function getATime();
    public function getMTime();
    public function getCTime();
    public function getType();
    public function isWritable();
    public function isReadable();
    public function isExecutable();
    public function isFile();
    public function isDir();
    public function isLink();
    public function getLinkTarget();
    public function getRealPath();

    /**
     * @param class_name[optional]
     */
    public function getFileInfo($class_name = null);

    /**
     * @param class_name[optional]
     */
    public function getPathInfo($class_name = null);

    /**
     * @param open_mode[optional]
     * @param use_include_path[optional]
     * @param context[optional]
     */
    public function openFile($open_mode = null, $use_include_path = null, $context = null);

    /**
     * @param class_name[optional]
     */
    public function setFileClass($class_name = null);

    /**
     * @param class_name[optional]
     */
    public function setInfoClass($class_name = null);
}
