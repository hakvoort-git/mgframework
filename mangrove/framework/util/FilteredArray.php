<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage grove
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

namespace mg;

use ArrayIterator;

class FilteredArray extends ArrayIterator {

    const DEFAULT_FILTER = 'escape';

    private $filter = null;

    public function __construct($array) {
        parent :: __construct($array);

        $this->filter = in(function(FilterProvider $filterProvider) {
            return $filterProvider->getFilter(FilteredArray :: DEFAULT_FILTER);
        });
    }

    public function setFilter($name) {
        $this->filter = in(function(FilterProvider $filterProvider) use ($name) {
            return $filterProvider->getFilter($name);
        });
    }

    public function getArrayCopy() {
        $filter = $this->filter;

        return array_map(function($element) use ($filter) { return $filter->filter($element); }, parent :: getArrayCopy());
    }

    /**
     * @param offset
     */
    public function offsetGet ($offset) {
        return $this->filter->filter(parent :: offsetGet($offset));
    }

    public function offsetSet ($offset, $value) {
        return $this->filter->filter(parent :: offsetSet($offset, $value));
    }

    public function current() {
        return $this->filter->filter(parent :: current());
    }
}