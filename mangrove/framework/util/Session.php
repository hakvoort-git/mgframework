<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage grove
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

namespace mg;

use ArrayAccess;

interface SessionManager {

    /**
     * @return Session
     */
    public function openSession();

    public function closeSession(Session $session);

    public function destroySession(Session $session);

    public function regenerateSessionId(Session $session);
}

class DefaultSessionManager implements SessionManager {

    /**
     * @var CookieManager
     */
    private $cookieManager = null;

    /**
     * @var HttpRequest
     */

    private $httpRequest = null;

    /**
     * @var HttpResponse
     */
    private $httpResponse = null;

    private $session = null;

    /**
     * @var boolean
     */
    private $isRegenerated = false;

    public function __construct(CookieManager $cookieManager, HttpRequest $httpRequest, HttpResponse $httpResponse) {
        $this->cookieManager = $cookieManager;
        $this->httpRequest = $httpRequest;
        $this->httpResponse = $httpResponse;
    }

    public function openSession() {

        if($this->session !== null) {
            return $this->session;
        }

        $sessionId = $this->cookieManager->getCookie('mgsession');

        $newSession = $sessionId === null;

        if(!$newSession) {
            session_id($sessionId);
        }

        session_name('mgsession');

        $params = session_get_cookie_params();

        // enforce http only
        session_set_cookie_params($params['lifetime'], $params['path'], $params['domain'], $params['secure'], true);

        session_cache_limiter('nocache');

        $this->httpResponse->emitResponseHeaders(function() {
            session_start();
        });

        $session = new DefaultSession($newSession, $this);

        // check for session normally generated bit
        $possibleFixation = !isset($session['s.known_id']);

        $remoteAddress = $this->httpRequest->getRemoteAddress();
        $userAgent = $this->httpRequest->getUserAgent();

        $sessionKey = md5($remoteAddress . $userAgent);

        // check whether the user agent or remote address has changed
        $possibleHijack = (!isset($session['s.key'])) || ($session['s.key'] !== $sessionKey);

        // Something is not right with the session, force a clean start
        if($possibleHijack || $possibleFixation) {
            $this->httpResponse->emitResponseHeaders(function() {
                session_destroy();
                session_start();
                session_regenerate_id(true);
            });

            $session = new DefaultSession(true, $this);
        }

        $session['s.key'] = $sessionKey;
        $session['s.known_id'] = true;

        $this->session = $session;

        return $this->session;
    }

    public function closeSession(Session $session) {
        $this->httpResponse->emitResponseHeaders(function() {
            session_write_close();
        });
    }

    public function regenerateSessionId(Session $session) {
        // No need to regenerate a session id for a new session or to re-regenerate a session id
        if($this->isRegenerated || $session->isNew()) {
            return;
        }

        $this->isRegenerated = true;

        $this->httpResponse->emitResponseHeaders(function() {
            session_regenerate_id(true);
        });
    }

    public function destroySession(Session $session) {
        $this->httpResponse->emitResponseHeaders(function() {
            session_destroy();
        });

        $this->cookieManager->invalidateCookie('mgsession');
    }
}

interface Session extends ArrayAccess {

    public function destroy();
    public function regenerateId();
    public function isNew();

}

class DefaultSession implements Session {

    private $sessionManager = null;

    private $isNew = false;

    public function __construct($isNew, SessionManager $sessionManager) {
        $this->isNew = $isNew;
        $this->sessionManager = $sessionManager;
    }

    public function destroy() {
        $this->sessionManager->destroySession($this);
    }

    public function regenerateId() {
        $this->sessionManager->regenerateSessionId($this);
    }

    public function isNew() {
        return $this->isNew;
    }

    public function offsetExists($offset) {
        return isset($_SESSION[$offset]);
    }

    public function offsetGet($offset) {
        return $_SESSION[$offset];
    }

    public function offsetSet($offset, $value) {
        $_SESSION[$offset] = $value;
    }

    public function offsetUnset($offset) {
        unset($_SESSION[$offset]);
    }
}

