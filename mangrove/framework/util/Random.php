<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage grove
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 * Mersenne twister pseudorandom number generator
 *
 */

namespace mg;

class Random {

    private $mt = null;
    private $read = 0;

    public function __construct($seed = null) {
        if($seed === null) {
            $seed = rand();
        }

        $this->mt = array_fill(0, 624, 0);
        $this->mt[0] = $seed;

        for($i = 1; $i < 624; $i++) {
            $this->mt[$i] = 0xffffffff & ((0x6c078965 * ($this->mt[$i-1] ^ ($this->mt[$i-1] >> 30))) + $i);
        }

    }

    private function generateNumbers() {
        for($i = 0; $i < 624; $i++) {
            $y = ($this->mt[$i] & 0x80000000) | ($this->mt[($i+1) % 624] & 0x7fffffff);
            $this->mt[$i] = $this->mt[($i + 397) % 624] ^ ($y >> 1);

            if(($y & 0x00000001) === 1) {
                $this->mt[$i] = $this->mt[$i] ^ 0x9908b0df;
            }
        }
    }

    public function nextBoolean() {
        return 0 === ($this->next() >> 30);
    }

    /**
     * Return the next integer value between 0 (inclusive) and 2^31 - 1 (inclusive).
     * If max is given, return the next integer value between 0 (inclusive) and max (exclusive)
     *
     * @param int $max
     * @return int
     */
    public function nextInt($max = false) {
        if($max !== false) {
            return $this->next() % $max;
        } else {
            return $this->next();
        }
    }

    /**
     * Return a 31-bit random integer
     *
     * @return int
     */
    private function next() {
        if($this->read === 0) {
            $this->generateNumbers();
        }


        $y = $this->mt[$this->read];
        $y ^= $y >> 11;
        $y ^= ($y << 7) & 0x9d2c5680;
        $y ^= ($y << 15) & 0xefc60000;
        $y ^= ($y >> 18);

        $this->read = ($this->read + 1) % 624;

        return $y >> 1;
    }
}
