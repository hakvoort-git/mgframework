<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage grove
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

namespace mg;

use Closure;

interface InternalHttpResponse {

    public function getResponseHeaders();
    public function getResponseBody();

}


interface HttpResponse {

    public function emitResponseHeaders(Closure $emitter);

    public function emitResponseBody(Closure $emitter);

}

class DefaultHttpResponse implements HttpResponse {

    public function __construct() {

    }

    public function emitResponseHeaders(Closure $emitter) {
        $emitter();
    }

    public function emitResponseBody(Closure $emitter) {
        $emitter();
    }

}

class CaptureHttpResponse implements HttpResponse, InternalHttpResponse {

    private $headers = null;

    /**
     * @var CaptureBuffer
     */
    private $buffer = null;

    public function __construct(Closure $emitter = null) {
        $this->headers = array();
        $this->buffer = new CaptureBuffer();

        if($emitter !== null) {
            $response = $this;

            $this->emitResponseBody(function() use($emitter, $response) {
                $response->emitResponseHeaders($emitter);
            });
        }



    }

    public function emitResponseHeaders(Closure $emitter) {
        // get previously set headers
        $headers = headers_list();

        $emitter();

        // slice added headers from previously set headers
        $addedHeaders = array_slice(headers_list(), count($headers));

        // store headers internally
        $this->headers = array_merge($this->headers, $addedHeaders);

        // restore previously set headers
        header_remove();

        foreach($headers as $header) {
            header($header, false);
        }
    }

    public function emitResponseBody(Closure $emitter) {
        $buffer = $this->buffer;

        $e = null;

        ob_start(function($text) use($buffer) {
            $buffer->write($text);
            return '';
        }, 4096);

        try {
            $emitter();
        } catch(\Exception $e) {
        }

        ob_end_clean();

        if($e) {
            throw $e;
        }

    }

    public function getResponseHeaders() {
        return $this->headers;
    }

    public function getResponseBodyBuffer() {
        return $this->buffer;
    }

    public function getResponseBody() {
        return $this->buffer->getContent();
    }
}

class HttpForward implements Viewable, View, InternalHttpResponse {

    private $headers = array();

    public function __construct($location) {
        $this->headers[] = "Location: {$location}";
    }

    public function getResponseHeaders() {
        return $this->headers;
    }

    public function getResponseBody() {
        return $this;
    }

    public function getView() {
        return $this;
    }

    public function write($object, Buffer $targetBuffer) {

    }
}

class AccessDeniedResponse implements Viewable, View, InternalHttpResponse {

    private $headers = array();

    public function __construct() {
        $this->headers[] = "HTTP/1.1 403 Access Denied";
    }

    public function getResponseHeaders() {
        return $this->headers;
    }

    public function getResponseBody() {
        return $this;
    }

    public function getView() {
        return $this;
    }

    public function write($object, Buffer $targetBuffer) {
        $targetBuffer->write("<html><body><h1>Access Denied</h1></body></html>");
    }
}

class PageNotFoundResponse implements Viewable, View, InternalHttpResponse {

    private $headers = array();

    public function __construct() {
        // TODO Fix this
        $this->headers[] = "HTTP/1.1 404 Not Found";
    }

    public function getResponseHeaders() {
        return $this->headers;
    }

    public function getResponseBody() {
        return $this;
    }

    public function getView() {
        return $this;
    }

    public function write($object, Buffer $targetBuffer) {
        $targetBuffer->write("<html><body><h1>Page not found</h1></body></html>");
    }
}

class ServerErrorResponse implements Viewable, View, InternalHttpResponse {
    private $headers = array();

    private $message = null;

    public function __construct($message) {
        $this->headers[] = "HTTP/1.1 500 Internal error";
        $this->message = $message;
    }

    public function getResponseHeaders() {
        return $this->headers;
    }

    public function getResponseBody() {
        return $this->message;
    }

    public function getView() {
        return $this;
    }

    public function write($object, Buffer $targetBuffer) {
        $targetBuffer->write("<html><body><h1>Something went wrong</h1></body></html>");
    }
}

class MimeTypeModifier implements InternalHttpResponse {

    private $mimeType;

    public function __construct($mimeType) {
        $this->mimeType = $mimeType;
    }

    public function getResponseBody() {
        return null;
    }

    public function getResponseHeaders() {
        return array("Content-type: {$this->mimeType}");
    }
}

class EmptyHttpResponse implements InternalHttpResponse {

    public function getResponseHeaders() {
        return array();
    }

    public function getResponseBody() {
        return "";
    }
}