<?php

namespace mg;

/**
 *
 * A temporary global storage for template variables
 *
 * @author Michiel Hakvoort <michiel@hakvoort.it>
 *
 */
class TemplateGlobal {

    public $values;

    public function __construct() {
        $this->value = array();
    }

    public function __set($key, $value) {
        $this->values[$key] = $value;
    }

    public function __get($key) {
        if(isset($this->values[$key])) {
            return $this->values[$key];
        }
        return null;
    }

    public static function resolveInstance() {
        return new TemplateGlobal();
    }
}
