<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage groove
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

namespace mg;


class GrooveToken extends Token {

    const T_MATCH_SEPARATOR = 13;
    const T_CLASS_SEPARATOR = 14;
    const T_CACHE = 15;
    const T_INLINE_PHP = 16;
    const T_PIPE = 17;
    const T_SCOPE = 18;
    const T_EQUALS = 19;
    const T_CONTEXT = 20;

    private $token;
    private $value;

    private static $names = array(
        self :: T_CURLY_CLOSE => 'T_CURLY_CLOSE'
        ,self :: T_PARENTHESIS_OPEN => 'T_PARENTHESIS_OPEN'
        ,self :: T_PARENTHESIS_CLOSE => 'T_PARENTHESIS_CLOSE'
        ,self :: T_CHARACTER => 'T_CHARACTER'
        ,self :: T_ASSIGNMENT => 'T_ASSIGNMENT'
        ,self :: T_OPERATOR => 'T_OPERATOR'
        ,self :: T_COLON => 'T_COLON'
        ,self :: T_SEMICOLON => 'T_SEMICOLON'
        ,self :: T_BRACKET_OPEN => 'T_BRACKET_OPEN'
        ,self :: T_BRACKET_CLOSE => 'T_BRACKET_CLOSE'
        ,self :: T_COMMA => 'T_COMMA'
        ,self :: T_DOUBLE_QUOTE => 'T_DOUBLE_QUOTE'
        ,self :: T_SINGLE_QUOTE => 'T_SINGLE_QUOTE'
        ,self :: T_MATCH_SEPARATOR => 'T_MATCH_SEPARATOR'
        ,self :: T_CLASS_SEPARATOR => 'T_CLASS_SEPARATOR'
        ,self :: T_CACHE => 'T_CACHE'
        ,self :: T_INLINE_PHP => 'T_INLINE_PHP'
        ,self :: T_PIPE => 'T_PIPE'
        ,self :: T_SCOPE => 'T_SCOPE'
        ,self :: T_EQUALS => 'T_EQUALS'
        ,self :: T_CONTEXT => 'T_CONTEXT');

    public function __construct($token, $value) {
        $this->token = intval($token);
        $this->value = $value;
    }

    public function __toString() {
        return (self :: getName($this->token)) . '(' . $this->value . ')';
    }

    public function getIdent() {
        return $this->token;
    }

    public function getValue() {
        return $this->value;
    }

    public static function getName($token) {
        $name = token_name($token);
        if($name === 'UNKNOWN' && array_key_exists($token, self :: $names)) {
            $name = self :: $names[$token];
        }
        return $name;
    }
}
