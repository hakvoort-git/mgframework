<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage groove
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

namespace mg;

class TranslateLexer extends Lexer {

    public function tokenize() {
        $token = null;
        $buffer = null;
        $allowSeparator = false;

        while(($next = $this->inputStream->read()) != -1) {
            if($buffer != null) {
                if($next == '_' || ctype_alnum($next)) {
                    $buffer .= $next;
                    continue;
                } else {
                    $this->parser->parse(new GrooveToken(GrooveToken :: T_STRING, $buffer));
                    $buffer = null;
                    $allowSeparator = true;
                }
            }

            // Expecting a symbol
            if(!$allowSeparator && ($next == '_' || ctype_alpha($next))) {
                $buffer = $next;
                continue;
            }

            // Fetch the expression
            if($next == '$') {
                $lexer = new ExpressionLexer($this->inputStream, $this->parser);
                $lexer->tokenize();
                $allowSeparator = true;
                continue;
            }

            // Good ol' separator
            if($allowSeparator && $next == ',') {
                $this->parser->parse(new GrooveToken(GrooveToken :: T_COMMA, ','));
                $allowSeparator = false;
                continue;
            }

            // Recursion
            if(!$allowSeparator && $next == '(') {
                $this->parser->parse(new GrooveToken(GrooveToken :: T_PARENTHESIS_OPEN, '('));
                $lexer = new TranslateLexer($this->inputStream, $this->parser);
                $lexer->tokenize();
                $allowSeparator = true;
                continue;
            }

            // End recursion
            if($allowSeparator && $next === ')') {
                $this->parser->parse(new GrooveToken(GrooveToken :: T_PARENTHESIS_CLOSE, ')'));

                $sink = new TokenSink();

                $lexer = new PipeLexer($this->inputStream, $sink);

                $offset = $this->inputStream->offset();

                try {
                    $lexer->tokenize();
                    $sink->clear($this->parser);
                } catch(TokenException $e) {
                    $offset2 = $this->inputStream->offset();
                    $this->inputStream->regress(($offset2 - $offset) );
                }
                return;
            }

            if(ctype_space($next)) {
                continue;
            }

            throw new TokenException('Unexpected character \'' . $next . '\'');
        }

        return $token;
    }

}