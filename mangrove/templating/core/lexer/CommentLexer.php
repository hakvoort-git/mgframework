<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage groove
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

namespace mg;

class CommentLexer extends Lexer {

    public function tokenize() {
        $token = null;
        $buffer = '';

        $type = null;

        while(($next = $this->inputStream->read()) != -1) {
            if($type == 'line') {
                $buffer .= $next;
                if($next == "\r" || $next == "\n") {
                    $this->parser->parse(new GrooveToken(GrooveToken :: T_COMMENT, mb_substr($buffer,0,-1)));
                    return;
                }
                continue;
            }

            if($type == 'block') {
                $buffer .= $next;
                if(mb_strlen($buffer) >= 2 && mb_substr($buffer, -2) == '*/') {
                    $this->parser->parse(new GrooveToken(GrooveToken :: T_DOC_COMMENT, mb_substr($buffer,0,-2)));
                    return;
                }
                continue;
            }

            if($next == '*') {
                $type = 'block';
                continue;
            } elseif($next == '/') {
                $type = 'line';
                continue;
            }

            throw new TokenException('Unexpected character \'' . $next . '\'');
        }
    }
}
