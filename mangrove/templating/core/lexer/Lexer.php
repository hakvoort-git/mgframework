<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage groove
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

namespace mg;

/**
 * Strip all whitespace left and right if and only
 *
 * @param string $in
 * @return stirng
 */

function trimline($in) {
    return ltrimline(rtrimline($in));
}

function ltrimline($in) {
    $leftLF = mb_strpos($in, "\n");

    if($leftLF === false) {
        return $in;
    }

    $isEmptyLine = true;

    $i = 0;
    while($isEmptyLine && $i < $leftLF) {
        $isEmptyLine = ctype_space($in{$i});
        $i++;
    }

    return $isEmptyLine ? mb_substr($in, $leftLF + 1) : $in;
}

function rtrimline($in) {
    $rightLF = mb_strrpos($in, "\n");

    if($rightLF === false) {
        return $in;
    }

    $isEmptyLine = true;
    $i = mb_strlen($in)-1;

    while($isEmptyLine && $i > $rightLF) {
        $isEmptyLine = ctype_space($in{$i});
        $i--;
    }

    return $isEmptyLine ? mb_substr($in, 0, $rightLF) : $in;
}

abstract class Lexer {

    /**
     * Enter description here...
     *
     * @var InputStream
     */
    protected $inputStream;

    /**
     * Enter description here...
     *
     * @var mgParser
     */
    protected $parser;

    public function __construct(InputStream $inputStream, GrooveParser $parser) {
        $this->inputStream = $inputStream;
        $this->parser = $parser;
    }

    public abstract function tokenize();
}

