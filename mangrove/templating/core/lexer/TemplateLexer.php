<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage groove
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

namespace mg;

class TemplateLexer extends Lexer {

    private static $escapes = array('n'=>"\n", 't'=>"\t", 'r'=>"\r");

    private $preTrim = true;

    public function tokenize() {
        $escape = false;
        $escapeNL = false;
        $escapeTemplate = false;
        $buffer = '';
        $acceptElse = false;
        $acceptElseIf = false;
        $vacuum = false;
        $doubleBuffer = '';

        while(($next = $this->inputStream->read()) !== -1) {
            // Add an escaped character
            if($escape) {
                if($escapeNL) {
                    if($doubleBuffer === "\r") {
                        $escape = false;
                        $escapeNL = false;
                        if($next === "\n") {
                            continue;
                        }
                    } elseif($next === "\n") {
                        $escape = false;
                        $escapeNL = false;
                        continue;
                    }
                    $doubleBuffer = $next;
                    continue;
                } elseif($vacuum) {
                    if(ctype_space($next)) {
                        continue;
                    } else {
                        $vacuum = false;
                        $escape = false;
                    }
                } elseif($escapeTemplate) {
                    if($doubleBuffer === '\\' && $next == 'e') {
                        $escapeTemplate = false;
                        $escape = false;
                        continue;
                    }
                    $buffer .= $doubleBuffer;
                    $doubleBuffer = $next;
                    continue;
                } else {
                    // Empty operator, ignore everything till end of line
                    if(ctype_space($next)) {
                        $escapeNL = true;
                        $doubleBuffer = $next;
                        continue;
                    }

                    // Vacuum operator, clear whitespace left and right
                    if($next === 'v') {
                        $buffer = rtrim($buffer);
                        $vacuum = true;
                        continue;
                    }

                    // Escape operator, escape from normal template parsing until next \e
                    if($next === 'e') {
                        $escapeTemplate = true;
                        $doubleBuffer = '';
                        continue;
                    }
                    	
                    if(isset(self :: $escapes[$next])) {
                        $buffer .= self :: $escapes[$next];
                    } else {
                        $buffer .= $next;
                    }

                    $escape = false;

                    continue;
                }
            }

            // Close the template
            if($next === '}') {
                $buffer = $this->preTrim ? trimline($buffer) : rtrimline($buffer);

                if(mb_strlen($buffer) > 0) {
                    $this->parser->parse(new GrooveToken(GrooveToken :: T_INLINE_HTML, $buffer));
                }
                $this->parser->parse(new GrooveToken(GrooveToken :: T_CURLY_CLOSE, $next));
                return;
            }

            // Parse an expression
            if($next === '$') {
                $this->inputStream->mark();

                $sink = new TokenSink();
                $lexer = new ExpressionLexer($this->inputStream, $sink);

                try {
                    $lexer->tokenize();
                    $buffer = $this->preTrim ? ltrimline($buffer) : $buffer;

                    if(mb_strlen($buffer) > 0) {
                        $this->parser->parse(new GrooveToken(GrooveToken :: T_INLINE_HTML, $buffer));
                        $buffer = '';
                    }

                    $sink->clear($this->parser);
                    $this->preTrim = false;
                    $acceptElse = false;
                    continue;
                } catch(TokenException $e) {
                    $this->inputStream->reset();
                }
            }

            // Parse a translation
            if($next === '(') {
                $this->inputStream->mark();

                $sink = new TokenSink();
                $lexer = new TranslateLexer($this->inputStream, $sink);

                try {
                    $lexer->tokenize();
                    $buffer = $this->preTrim ? ltrimline($buffer) : $buffer;
                    if(mb_strlen($buffer) > 0) {
                        $this->parser->parse(new GrooveToken(GrooveToken :: T_INLINE_HTML, $buffer));
                        $buffer = '';
                    }
                    $this->preTrim = false;
                    $this->parser->parse(new GrooveToken(GrooveToken :: T_PARENTHESIS_OPEN, '('));
                    $sink->clear($this->parser);
                    $acceptElse = false;
                    continue;
                } catch(TokenException $e) {
                    $this->inputStream->reset();
                }
            }

            // Escape the next character
            if($next === '\\') {
                $escape = true;
                $acceptElse = false;
                continue;
            }

            // Increase the buffer
            $buffer .= $next;

            if(mb_strlen($buffer) >= 5 && mb_strtolower(mb_substr($buffer, -5)) === 'cache') {
                $this->inputStream->mark();
                $sink = new TokenSink();
                $lexer = new ControlStructureLexer($this->inputStream, $sink);
                try {
                    $lexer->tokenize();
                    $cache = mb_substr($buffer, -5);
                    $buffer = mb_substr($buffer, 0, -5);
                    $buffer = $this->preTrim ? trimline($buffer) : rtrimline($buffer);
                    $this->preTrim = true;
                    if(mb_strlen($buffer) > 0) {
                        $this->parser->parse(new GrooveToken(GrooveToken :: T_INLINE_HTML, $buffer));
                        $buffer = '';
                    }
                    $this->parser->parse(new GrooveToken(GrooveToken :: T_CACHE, $cache));
                    $sink->clear($this->parser);
                } catch(TokenException $e) {
                    $this->inputStream->reset();
                }
                continue;
            }

            if(mb_strlen($buffer) >= 1 && mb_strtolower(mb_substr($buffer, -1)) === '@') {
                $this->inputStream->mark();
                $sink = new TokenSink();
                $lexer = new ControlStructureLexer($this->inputStream, $sink);
                try {
                    $lexer->tokenize();
                    $cache = mb_substr($buffer, -1);
                    $buffer = mb_substr($buffer, 0, -1);
                    $buffer = $this->preTrim ? trimline($buffer) : rtrimline($buffer);
                    $this->preTrim = true;
                    if(mb_strlen($buffer) > 0) {
                        $this->parser->parse(new GrooveToken(GrooveToken :: T_INLINE_HTML, $buffer));
                        $buffer = '';
                    }
                    $this->parser->parse(new GrooveToken(GrooveToken :: T_SCOPE, $cache));
                    $sink->clear($this->parser);
                } catch(TokenException $e) {
                    $this->inputStream->reset();
                }
                continue;
            }

            if(mb_strlen($buffer) >= 4 && mb_strtolower(mb_substr($buffer, -4)) === 'with') {
                $this->inputStream->mark();
                $sink = new TokenSink();
                $lexer = new ControlStructureLexer($this->inputStream, $sink);
                try {
                    $lexer->tokenize();
                    $cache = mb_substr($buffer, -4);
                    $buffer = mb_substr($buffer, 0, -4);
                    $buffer = $this->preTrim ? trimline($buffer) : rtrimline($buffer);
                    $this->preTrim = true;
                    if(mb_strlen($buffer) > 0) {
                        $this->parser->parse(new GrooveToken(GrooveToken :: T_INLINE_HTML, $buffer));
                        $buffer = '';
                    }
                    $this->parser->parse(new GrooveToken(GrooveToken :: T_SCOPE, $cache));
                    $sink->clear($this->parser);
                } catch(TokenException $e) {
                    $this->inputStream->reset();
                }
                continue;
            }

            if(mb_strlen($buffer) >= 7 && mb_strtolower(mb_substr($buffer, -7)) === 'foreach') {
                $this->inputStream->mark();
                $sink = new TokenSink();
                $lexer = new ControlStructureLexer($this->inputStream, $sink);
                try {
                    $lexer->tokenize();
                    $cache = mb_substr($buffer, -7);
                    $buffer = mb_substr($buffer, 0, -7);
                    $buffer = $this->preTrim ? trimline($buffer) : rtrimline($buffer);
                    $this->preTrim = true;
                    if(mb_strlen($buffer) > 0) {
                        $this->parser->parse(new GrooveToken(GrooveToken :: T_INLINE_HTML, $buffer));
                        $buffer = '';
                    }
                    $this->parser->parse(new GrooveToken(GrooveToken :: T_FOREACH, $cache));
                    $sink->clear($this->parser);
                    $acceptElse = true;
                    $acceptElseIf = false;
                } catch(TokenException $e) {
                    $this->inputStream->reset();
                }
                continue;
            }

            if($acceptElse && mb_strlen($buffer) >= 4 && mb_strtolower(mb_substr($buffer, -4)) === 'else') {
                $this->inputStream->mark();
                $last = $this->inputStream->read();
                $gotCurly = false;
                while($last != -1 && !$gotCurly) {
                    if(!ctype_space($last)) {
                        $gotCurly = $last == '{';
                        if(!$gotCurly) {
                            $last = -1;
                        }
                    } else {
                        $last = $this->inputStream->read();
                    }
                }

                if($gotCurly) {
                    $this->parser->parse(new GrooveToken(GrooveToken :: T_ELSE, mb_substr($buffer, -4)));
                    $buffer = '';
                    $this->parser->parse(new GrooveToken(GrooveToken :: T_CURLY_OPEN, '{'));
                    $acceptElse = false;
                    $acceptElseIf = false;
                    $lexer = new TemplateLexer($this->inputStream, $this->parser);
                    $lexer->tokenize();
                    continue;
                } else {
                    $this->inputStream->reset();
                }
            }

            if($acceptElseIf && mb_strlen($buffer) >= 6 && mb_strtolower(mb_substr($buffer, -6)) === 'elseif') {
                $this->inputStream->mark();
                $sink = new TokenSink();
                $lexer = new ControlStructureLexer($this->inputStream, $sink);
                try {
                    $lexer->tokenize();
                    $elseif = mb_substr($buffer, -6);
                    $buffer = '';
                    $this->preTrim = true;
                    $this->parser->parse(new GrooveToken(GrooveToken :: T_ELSEIF, $elseif));
                    $sink->clear($this->parser);
                } catch(TokenException $e) {
                    $this->inputStream->reset();
                }
                continue;
            }


            if(mb_strlen($buffer) >= 2 && mb_strtolower(mb_substr($buffer, -2)) === 'if') {
                $this->inputStream->mark();
                $sink = new TokenSink();
                $lexer = new ControlStructureLexer($this->inputStream, $sink);
                try {
                    $lexer->tokenize();
                    $if = mb_substr($buffer, -2);
                    $buffer = mb_substr($buffer, 0, -2);
                    $buffer = $this->preTrim ? trimline($buffer) : rtrimline($buffer);
                    $this->preTrim = true;
                    if(mb_strlen($buffer) > 0) {
                        $this->parser->parse(new GrooveToken(GrooveToken :: T_INLINE_HTML, $buffer));
                        $buffer = '';
                    }
                    $this->parser->parse(new GrooveToken(GrooveToken :: T_IF, $if));
                    $sink->clear($this->parser);
                    $acceptElse = true;
                    $acceptElseIf = true;
                } catch(TokenException $e) {
                    $this->inputStream->reset();
                }
                continue;
            }

            // Reset else / elseif
            if($acceptElse && mb_strlen(trim($buffer)) > 0) {
                $acceptElse = mb_strpos('elseif', mb_strtolower(trim($buffer))) === 0;
                $acceptElseIf = $acceptElseIf && $acceptElse;
            }

            if(mb_strlen($buffer) >= 5 && mb_strtolower(mb_substr($buffer, -5)) === '<?php') {
                $php = mb_substr($buffer, -5);
                $buffer = mb_substr($buffer, 0, -5);
                $buffer = $this->preTrim ? trimline($buffer) : rtrimline($buffer);
                if(mb_strlen($buffer) > 0) {
                    $this->parser->parse(new GrooveToken(GrooveToken :: T_INLINE_HTML, $buffer));
                }
                $buffer = '';
                $this->parser->parse(new GrooveToken(GrooveToken :: T_OPEN_TAG, $php));

                $lexer = new PhpLexer($this->inputStream, $this->parser);
                $lexer->tokenize();
                $this->preTrim = true;
                continue;
            }

            if(mb_strlen($buffer) >= 3 && mb_substr($buffer, -3) === '<?=') {
                $buffer = mb_substr($buffer, 0, -3);
                $buffer = $this->preTrim ? ltrimline($buffer) : $buffer;
                if(mb_strlen($buffer) > 0) {
                    $this->parser->parse(new GrooveToken(GrooveToken :: T_INLINE_HTML, $buffer));
                }
                $buffer = '';
                $this->parser->parse(new GrooveToken(GrooveToken :: T_OPEN_TAG_WITH_ECHO, '<?='));

                $lexer = new PhpLexer($this->inputStream, $this->parser);
                $lexer->tokenize();

                $this->preTrim = true;
                continue;
            }
        }
    }
}
