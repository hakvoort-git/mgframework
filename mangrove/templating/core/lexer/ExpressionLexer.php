<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage groove
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

namespace mg;

class ExpressionLexer extends Lexer {

    public function tokenize() {
        $token = null;
        $buffer = null;

        while(($next = $this->inputStream->read()) != -1) {
            if($buffer != null) {
                if($next == '_' || ctype_alnum($next)) {
                    $buffer .= $next;
                } else {
                    $this->parser->parse(new GrooveToken(GrooveToken :: T_VARIABLE, '$'.$buffer));
                    // accept a -> here
                    $sink = new TokenSink();

                    $lexer = new ComplexExpressionLexer($this->inputStream, $sink);
                    $this->inputStream->regress(1);
                    $offset = $this->inputStream->offset();
                    $this->inputStream->mark();

                    try {
                        $lexer->tokenize();
                        $sink->clear($this->parser);
                    } catch(TokenException $e) {
                        $this->inputStream->reset();
                    }

                    return;
                }
                continue;
            }

            if($next == '_' || ctype_alpha($next)) {
                $buffer = $next;
                continue;
            }

            throw new TokenException('Unexpected character \'' . $next . '\'');
        }

        return $token;
    }

}

class PipeLexer extends Lexer {

    public function tokenize() {
        $buffer = '';
        $gotPipe = false;

        while(($next = $this->inputStream->read()) !== -1) {
            // Auto left trim buffer
            if(mb_strlen($buffer) === 0 && ctype_space($next)) {
                continue;
            }

            // Search for the arrow first
            if(!$gotPipe) {
                if($next == '|') {
                    $gotPipe = true;
                    $this->parser->parse(new GrooveToken(GrooveToken :: T_PIPE, '|'));
                } else {
                    throw new TokenException("Unexpected character'$next'");
                }
                continue;
            }

            // Collect the word after the arrow or pipe
            if(mb_strlen($buffer) > 0 && (ctype_alnum($next) || $next === '_')) {
                $buffer .= $next;
                continue;
            }

            // Collect the word after the arrow or pipe
            if($next == '_' || ctype_alpha($next)) {
                $buffer = $next;
                continue;
            }

            // No word was found after the -> or |, throw an exception
            if(mb_strlen($buffer) === 0) {
                throw new TokenException("Unexpected character'".$next."'");
            }

            $this->parser->parse(new GrooveToken(GrooveToken :: T_STRING, $buffer));

            // got something like -> or | ab
            // in case of |, try to search for ( .. )
            // in case of ->, try to search for ( .. ) or ->

            $this->inputStream->regress(1);
            $this->inputStream->mark();

            try {
                $sink = new TokenSink();
                $lexer = new ComplexExpressionLexer($this->inputStream, $sink);
                $lexer->tokenize();
                $sink->clear($this->parser);
            } catch(TokenException $e) {
                $this->inputStream->reset();
                try {
                    $sink = new TokenSink();
                    $lexer = new ParenthesizedExpression($this->inputStream, $sink);
                    $lexer->tokenize();
                    $sink->clear($this->parser);
                } catch(TokenException $e) {
                    $this->inputStream->reset();
                }
            }
            return;
        }
        throw new TokenException("End Of File unexpected");
    }
}

class ComplexExpressionLexer extends Lexer {

    public function tokenize() {
        $buffer = '';
        $gotArrow = false;
        $gotPipe = false;
        while(($next = $this->inputStream->read()) != -1) {
            // Auto left trim buffer
            if(mb_strlen($buffer) === 0 && ctype_space($next)) {
                continue;
            }

            // Search for the arrow first
            if(!($gotArrow || $gotPipe)) {
                if(!$gotArrow) {
                    $buffer .= $next;
                    if(mb_strlen($buffer) > 2) {
                        throw new TokenException("Expected '->' or '|' got '".$buffer."'");
                    }
                    if($buffer === '->') {
                        $gotArrow = true;
                        $this->parser->parse(new GrooveToken(GrooveToken :: T_OBJECT_OPERATOR, '->'));
                        $buffer = '';
                    }
                }

                if(!$gotPipe) {
                    if($buffer === '|') {
                        $gotPipe = true;
                        $this->parser->parse(new GrooveToken(GrooveToken :: T_PIPE, '|'));
                        $buffer = '';
                    }
                }
                continue;
            }

            // Collect the word after the arrow or pipe
            if(($gotArrow || $gotPipe) && mb_strlen($buffer) > 0 && (ctype_alnum($next) || $next === '_')) {
                $buffer .= $next;
                continue;
            }

            // Collect the word after the arrow or pipe
            if(($gotArrow || $gotPipe) && $next == '_' || ctype_alpha($next)) {
                $buffer = $next;
                continue;
            }

            // No word was found after the -> or |, throw an exception
            if(mb_strlen($buffer) === 0) {
                throw new TokenException("Unexpected character'".$next."'");
            }

            $this->parser->parse(new GrooveToken(GrooveToken :: T_STRING, $buffer));

            // got something like -> or | ab
            // in case of |, try to search for ( .. )
            // in case of ->, try to search for ( .. ) or ->

            $this->inputStream->regress(1);
            $this->inputStream->mark();

            try {
                $sink = new TokenSink();
                $lexer = new ComplexExpressionLexer($this->inputStream, $sink);
                $lexer->tokenize();
                $sink->clear($this->parser);
            } catch(TokenException $e) {
                $this->inputStream->reset();
                try {
                    $sink = new TokenSink();
                    $lexer = new ParenthesizedExpression($this->inputStream, $sink);
                    $lexer->tokenize();
                    $sink->clear($this->parser);
                } catch(TokenException $e) {
                    $this->inputStream->reset();
                }
            }
            return;
        }
        throw new TokenException("End Of File unexpected");
    }
}

class ParenthesizedExpression extends Lexer {
    private $onObject;

    public function __construct(InputStream $inputStream, GrooveParser $parser, $onObject = true) {
        parent :: __construct($inputStream, $parser);
        $this->onObject = $onObject;
    }

    public function tokenize() {
        $depth = 0;
        $isEmpty = true;
        $buffer = '';

        $acceptClose = null;
        $escaping = false;

        while(($next = $this->inputStream->read()) != -1) {
            $buffer .= $next;
            if($acceptClose == '\'') {
                if(!$escaping && $next == '\\') {
                    $escaping = true;
                    continue;
                } else {
                    if(!$escaping && $next == '\'') {
                        $acceptClose = null;
                    }
                    $escaping = false;
                }
                continue;
            } elseif($acceptClose == '"') {
                if(!$escaping && $next == '\\') {
                    $escaping = true;
                    continue;
                } else {
                    if(!$escaping && $next == '"') {
                        $acceptClose = null;
                    }
                    $escaping = false;
                }
                continue;
            }

            if(!$isEmpty && ($next == '\'' || $next == '"')) {
                $acceptClose = $next;
                continue;
            }

            if($next == '(') {
                $isEmpty = false;
                $depth++;
                continue;
            }

            if($next == ')') {
                if($isEmpty) {
                    throw new TokenException('Unexpected character \')\'');
                }
                $depth--;
                if($depth == 0) {
                    $this->parser->parse(new GrooveToken(GrooveToken :: T_PARENTHESIS_OPEN, '('));
                    $tokens = @token_get_all('<?php '.mb_substr($buffer, 1, -1));

                    for($i = 1; $i < count($tokens); $i++) {
                        $token = $tokens[$i];
                        if(is_array($token)) {
                            $this->parser->parse(new GrooveToken($token[0], $token[1]));
                        } else {
                            $this->parser->parse(new GrooveToken(GrooveToken :: T_CHARACTER, $token));
                        }
                    }

                    $this->parser->parse(new GrooveToken(GrooveToken :: T_PARENTHESIS_CLOSE, ')'));
                    if($this->onObject) {
                        $this->inputStream->mark();
                        try {
                            $sink = new TokenSink();
                            $lexer = new ComplexExpressionLexer($this->inputStream, $sink);
                            $lexer->tokenize();
                            $sink->clear($this->parser);
                        } catch(TokenException $e) {
                            $this->inputStream->reset();
                        }
                    }
                    return;
                }
            }

            if($isEmpty) {
                throw new TokenException('Unexpected character \''.$next.'\'');
            }
        }
        throw new TokenException("End Of File unexpected");
    }
}
