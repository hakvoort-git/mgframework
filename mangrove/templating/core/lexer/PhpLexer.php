<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage groove
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

namespace mg;

class PhpLexer extends Lexer {

    public function tokenize() {
        $buffer = '';

        $acceptClose = null;
        $escaping = false;

        while(($next = $this->inputStream->read()) != -1) {
            $buffer .= $next;

            if($acceptClose !== null) {
                if(!$escaping && $next === '\\') {
                    $escaping = true;
                    continue;
                } else {
                    if(!$escaping && $next === $acceptClose) {
                        $acceptClose = null;
                    }
                    $escaping = false;
                }
                continue;
            }
            /*

            if($acceptClose == '\'') {
            if(!$escaping && $next == '\\') {
            $escaping = true;
            continue;
            } else {
            if(!$escaping && $next == '\'') {
            $acceptClose = null;
            }
            $escaping = false;
            }
            continue;
            } elseif($acceptClose == '"') {
            if(!$escaping && $next == '\\') {
            $escaping = true;
            continue;
            } else {
            if(!$escaping && $next == '"') {
            $acceptClose = null;
            }
            $escaping = false;
            }
            continue;
            } elseif($acceptClose === '`') {

            }*/

            if($next === '\'' || $next === '"' || $next === '`') {
                $acceptClose = $next;
                continue;
            }

            if(mb_substr($buffer, -2) === '?>') {
                $tokens = @token_get_all('<?php '.$buffer);
                for($i = 1; $i < count($tokens); $i++) {
                    $token = $tokens[$i];
                    if(is_array($token)) {
                        $this->parser->parse(new GrooveToken($token[0], $token[1]));
                    } else {
                        $this->parser->parse(new GrooveToken(GrooveToken :: T_CHARACTER, $token));
                    }
                }
                //				$this->parser->parse(new Token(Token :: T_INLINE_PHP, substr($buffer, 0, -2)));
                $this->parser->parse(new GrooveToken(GrooveToken :: T_CLOSE_TAG, '?>'));
                return;
            }
        }
    }

}
