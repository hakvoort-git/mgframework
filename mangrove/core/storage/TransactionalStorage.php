<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage core
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */


namespace mg;

use \BadMethodCallException;

/**
 * The TransactionalStorage acts as a Transactional layer for Storages. By opening
 * a transaction, the storage can be modified. Committing the transaction replays
 * the transaction into the backed Storage. Reading the underlying Storage can be
 * done outside of the transaction scope, but transactional writes require the
 * TransactionalStorage to be in a transaction;
 *
 * @author Michiel Hakvoort
 *
 */
class TransactionalStorage extends AbstractStorage implements Transactional {

    /**
     *
     * @var \mg\Storage
     */
    private $storage = null;

    private $inTransaction = false;

    /**
     *
     * @var array
     */
    private $transactionSet = null;

    /**
     *
     * @var array
     */
    private $transactionUnset = null;

    public function __construct(Storage $storage) {
        parent :: __construct($storage->getName());

        $this->storage = $storage;

        $this->transactionSet = array();
        $this->transactionUnset = array();

    }

    public function begin() {
        if($this->inTransaction) {
            throw new Exception("Storage already in transaction");
        }

        $this->transactionSet = array();
        $this->transactionUnset = array();
        $this->inTransaction = true;
    }

    public function commit() {
        if(!$this->inTransaction) {
            throw new Exception("No open transaction to commit");
        }
        // Perform write outs
        foreach($this->transactionSet as $key => $value) {
            $this->storage->put($key, $value);
        }

        // Removes
        foreach($this->transactionUnset as $key => $value) {
            $this->storage->offsetUnset($key);
        }

        $this->inTransaction = false;
        $this->transactionSet = array();
        $this->transactionUnset = array();
    }

    public function rollback() {
        if(!$this->inTransaction) {
            throw new Exception("No open transaction to roll back");
        }

        $this->inTransaction = false;
        $this->transactionSet = array();
        $this->transactionUnset = array();
    }

    private function checkTransaction() {
        if(!$this->inTransaction) {
            throw new Exception("Storage is not within an active transaction");
        }
    }

    public function offsetSet($offset, $value) {
        $this->checkTransaction();

        if(isset($this->transactionUnset[$offset])) {
            unset($this->transactionUnset[$offset]);
        }

        $this->transactionSet[$offset] = $value;
    }

    public function offsetExists($offset) {
        return !isset($this->transactionUnset[$offset]) && (isset($this->transactionSet[$offset]) || $this->storage->offsetExists($offset));
    }

    public function offsetGet($offset) {
        if(isset($this->transactionUnset[$offset])) {
            return false;
        }

        if(isset($this->transactionSet[$offset])) {
            return $this->transactionSet[$offset];
        }

        return $this->storage->offsetGet($offset);
    }

    public function offsetUnset($offset) {
        $this->checkTransaction();

        if(isset($this->transactionSet[$offset])) {
            unset($this->transactionSet[$offset]);
        }

        $this->transactionUnset[$offset] = true;
    }

    public function clear() {
        $this->checkTransaction();

        // Get all known offsets
        $offsets = $this->getOffsets();

        // Remove the current write set
        $this->transactionSet = array();

        // Fill the remove set with all offsets
        foreach($offsets as $offset) {
            $this->transactionUnset[$offset] = true;
        }

    }

    public function getOffsets() {
        $offsets = $this->storage->getOffsets();

        // TODO, reduce O(mn) to O(m) with m = #($this->storage)
        foreach($this->transactionUnset as $key => $value) {
            while(($offset = array_search($key, $offsets, true)) !== FALSE) {
                unset($offsets[$offset]);
            }
        }

        foreach($this->transactionSet as $key=>$value) {
            if(!in_array($key, $offsets, true)) {
                $offsets[] = $key;
            }
        }

        return $offsets;
    }

    public function current() {
        return new BadMethodCallException("current() not implemented");
    }

    public function key() {
        return new BadMethodCallException("key() not implemented");
    }

    public function next() {
        return new BadMethodCallException("next() not implemented");
    }

    public function rewind() {
        return new BadMethodCallException("rewind() not implemented");
    }

    public function valid() {
        return new BadMethodCallException("valid() not implemented");
    }

    public function count() {
        return new BadMethodCallException("count() not implemented");
    }
}



