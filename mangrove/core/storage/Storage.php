<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage core
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */


namespace mg;

use \ArrayAccess, \Iterator, \Countable;

interface Storage extends ArrayAccess, Iterator, Countable {

    public function get($key);
    public function put($key, $value);
    public function remove($key);
    public function contains($key);
    public function clear();
    public function getOffsets();
    public function getName();
}

abstract class AbstractStorage implements Storage {

    protected $storageName;

    public function __construct($storageName) {
        $this->storageName = $storageName;
    }

    public function get($key) {
        return $this[$key];
    }

    public function put($key, $value) {
        $this[$key] = $value;
    }

    public function remove($key) {
        unset($this[$key]);
    }

    public function contains($key) {
        return isset($this[$key]);
    }

    public function getName() {
        return $this->storageName;
    }
}

abstract class StorageProvider {

    private $storages = array();

    /**
     * Get a Storage with the given storage name. If a Storage with the name is already
     * in use, return that Storage.
     *
     * @param string $storageName
     * @return Storage
     */
    public final function getStorage($storageName) {
        $storageName = mb_strtolower($storageName);

        if(!isset($this->storages[$storageName])) {
            $this->storages[$storageName] = $this->createStorage($storageName);
        }

        return $this->storages[$storageName];
    }

    /**
     * Check whether a Storage with the given name has already been created.
     *
     * @param string $storageName
     * @return Storage
     */
    public final function hasStorage($storageName) {
        return isset($this->storages[mb_strtolower($storageName)]);
    }

    /**
     * Create a new Storage with the given storage name.
     *
     * @param string $storageName
     * @return Storage
     */
    public abstract function createStorage($storageName);
}

class StorageException extends Exception {

}
