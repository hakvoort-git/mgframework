<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage core
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

namespace mg;

class CacheStorage extends AbstractStorage {

    /**
     * @var Storage
     */
    private $storage;

    private $key;

    /**
     * Enter description here...
     *
     * @var Cache
     */
    private $cache;

    public function __construct(Storage $storage, Cache $cache, $key) {
        parent :: __construct($storage->getName());

        $this->storage = $storage;
        $this->cache = $cache;
        $this->key = $key;
    }

    public function offsetSet($offset, $value) {
        $this->storage->offsetSet($offset, $value);

        $this->cache[$this->key . $offset] = $value;
    }

    public function offsetExists($offset) {
        // TODO investigate caching
        if(isset($this->cache[$this->key . $offset])) {
            return $this->cache[$this->key . $offset] !== false;
        }

        $result = $this->storage->offsetExists($offset);

        // Write false in cache when the value is not set in the internal storage
        if($result === false) {
            $this->cache[$this->key . $offset] = false;
        }

        return $result;
    }

    public function offsetGet($offset) {
        $result = false;

        if(isset($this->cache[$this->key . $offset])) {
            return $this->cache[$this->key . $offset];
        }

        $result = $this->storage->offsetGet($offset);
        $this->cache[$this->key . $offset] = $result;
        return $result;
    }

    public function offsetUnset($offset) {
        unset($this->cache[$this->key . $offset]);

        $this->storage->offsetUnset($offset);
    }

    public function clear() {
        $offsets = $this->getOffsets();

        foreach($offsets as $offset) {
            unset($this->cache[$this->key . $offset]);
        }

        $this->storage->clear();
    }

    public function getOffsets() {
        return $this->storage->getOffsets();
    }


    public function current() {
        return $this->storage->current();
    }

    public function key() {
        return $this->storage->key();
    }

    public function next() {
        return $this->storage->next();
    }

    public function rewind() {
        return $this->storage->rewind();
    }

    public function valid() {
        return $this->storage->valid();
    }

    public function count() {
        return $this->storage->count();
    }
}