<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage core
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 * An abstract storage engine which is backed by php's internal array type, or
 * by an implementation of the ArrayObject class.
 * An implementation of mgArrayBackingStorage is responsible for providing a
 * read / write mechanism for the internal array. Because of direct object
 * serialization it takes a performance penalty for reading and writing
 * non-scalars.
 *
 */

namespace mg;

abstract class ArrayBackingStorage extends AbstractStorage {

    // The actual data within the storage
    protected $storage = null;

    // If true, the backing array only contains scalars
    protected $onlyScalar = false;

    // Invariant, if true the Storage is modified
    private $isModified = false;

    private $isValid = false;

    public function __construct($storageName) {
        parent :: __construct($storageName);
    }

    /**
     * If the Storage has been modified, write it to the file
     */
    public function __destruct() {
        // Don't do anything if the storage is not touched
        if($this->storage == null) {
            return;
        }

        // Don't store deserialized entries

        $onlyScalar = true;
        if(!$this->onlyScalar) {
            foreach($this->storage as $key => &$entry) {
                if(array_key_exists('cache', $entry)) {
                    $serialized = serialize($entry['cache']);
                    if($entry['value'] !== $serialized) {
                        $entry['value'] = $serialized;
                        $this->isModified = true;
                        $onlyScalar = false;
                    }
                    unset($entry['cache']);
                }
            }
        }

        $this->onlyScalar = $onlyScalar;

        if($this->isModified) {
            $this->writeBackingArray($this->storage, $this->onlyScalar);
        }
    }

    public function getOffsets() {
        $this->initBackingArray();
        return array_keys($this->storage);
    }

    private function initBackingArray() {
        if($this->storage === null) {
            $this->readBackingArray();
        }
    }

    protected abstract function readBackingArray();
    protected abstract function writeBackingArray($storage, $onlyScalar);

    /**
     * Store a value in the backing store
     * Storing a value is in O(1)
     */
    public function offsetSet($offset, $value) {
        $this->initBackingArray();
        // Don't store when the old and the new value are equal
        if(isset($this->storage[$offset])) {
            if(!$this->storage[$offset]['isSerialized']) {
                if($this->storage[$offset]['value'] === $value) {
                    return $value;
                }
            } else {
                if(serialize($value) === $this->storage[$offset]['value']) {
                    return $value;
                }
            }
        }
        $entry = array();

        if($this->onlyScalar($value)) {
            $entry['isSerialized']	= false;
            $entry['value']			= $value;
        } else {
            $entry['isSerialized']	= true;
            $entry['value']			= serialize($value);
            $entry['cache']			= $value;
            $this->onlyScalar		= false;
        }

        $this->storage[$offset] = $entry;

        $this->isModified = true;

        return $value;
    }

    /**
     * Return true when a $val is a scalar or $val is an array containing just scalars
     *
     * // TODO:  prevent pass by reference recursion
     */
    private function onlyScalar($val) {
        if(is_scalar($val)) {
            return true;
        } elseif(is_array($val)) {
            $onlyScalar = true;
            reset($val);
            while($onlyScalar && ($pair = each($val))) {
                $onlyScalar = $this->onlyScalar($pair['value']);
            }
            return $onlyScalar;
        } else {
            return false;
        }
    }

    /**
     * Retrieve an entry from the Storage
     * Retrieving is in O(1)
     */
    public function offsetGet($offset) {
        $this->initBackingArray();

        if(!isset($this->storage[$offset])) {
            return false;
        }

        $entry =& $this->storage[$offset];
        if(!$entry['isSerialized']) {
            return $entry['value'];
        } else {
            if(!isset($entry['cache'])) {
                $entry['cache'] = unserialize($entry['value']);
            }
            return $entry['cache'];
        }
    }

    /**
     * Check whether an entry is present in the Storage
     * Checking is in O(1)
     */
    public function offsetExists($offset) {
        $this->initBackingArray();
        return isset($this->storage[$offset]);
    }

    /**
     * Remove an entry from the Record and update the scalar status of the Record.
     * Removing is in O(1)
     */
    public function offsetUnset($offset) {
        $this->initBackingArray();
        if(isset($this->storage[$offset])) {
            unset($this->storage[$offset]);
            $this->isModified = true;
        }
    }

    public function clear() {
        $this->storage = array();
        $this->isModified = true;
        $this->onlyScalar = true;
    }

    public function isEmpty() {
        $this->initBackingArray();
        return count($this->storage) == 0;
    }

    public function current() {
        $this->initBackingArray();
        return $this->offsetGet($this->key());
    }

    public function key() {
        $this->initBackingArray();
        return key($this->storage);
    }

    public function next() {
        $this->initBackingArray();
        $this->isValid = next($this->storage);
    }

    public function rewind() {
        $this->initBackingArray();
        $this->isValid = true;
        reset($this->storage);
    }

    public function valid() {
        $this->initBackingArray();
        return $this->isValid;
    }

    public function count() {
        $this->initBackingArray();
        return count($this->storage);
    }
}