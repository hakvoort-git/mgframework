<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage core
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 * An implementation which delegates reads/writes to the filesystem
 *
 */

namespace mg;

class FileSystemStorage extends ArrayBackingStorage {

    private $file = null;

    public function __construct($storageName, $root) {
        parent :: __construct($storageName);

        $this->file = $root . DS . $storageName;
    }

    protected function readBackingArray() {
        if(file_exists($this->file)) {
            $package = unserialize(file_get_contents($this->file));
            $this->storage = $package['storage'];
            $this->onlyScalar = $package['onlyScalar'];
        } else {
            $this->storage = array();
            $this->onlyScalar = true;
        }
    }

    protected function writeBackingArray($storage, $onlyScalar) {
        $package = array('storage' => $storage, 'onlyScalar' => $onlyScalar);
        file_put_contents($this->file, serialize($package));
    }

}


class FileSystemStorageProvider extends StorageProvider {

    private $root = null;

    public function __construct($root) {
        $this->root = $root;
    }

    public function createStorage($storageName) {
        return new FileSystemStorage($storageName, $this->root);
    }

}
