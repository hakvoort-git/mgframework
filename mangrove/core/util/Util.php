<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage core
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

namespace mg {

    use \RecursiveFilterIterator, \RecursiveDirectoryIterator, \ReflectionClass;

    class RegexRecursiveDirectoryFilterIterator extends RecursiveFilterIterator {

        private $directoryRegex = null;
        private $fileRegex = null;

        public function __construct(RecursiveDirectoryIterator $iterator, $directoryRegex, $fileRegex) {
            parent :: __construct($iterator);
            $this->directoryRegex = $directoryRegex;
            $this->fileRegex = $fileRegex;
        }

        public function accept() {
            $filename = $this->getFilename();
            if($this->isDir()) {
                return preg_match($this->directoryRegex, $filename) > 0;
            } else {
                return preg_match($this->fileRegex, $filename) > 0;
            }
        }

        public function getChildren() {
            if (empty($this->ref)) {
                $this->ref = new ReflectionClass($this);
            }
            return $this->ref->newInstance($this->getInnerIterator()->getChildren(), $this->directoryRegex, $this->fileRegex);
        }

    }

    /**
     * An instantiable representation of nothing
     *
     * @author Michiel Hakvoort
     */
    final class Unit {
        private static $unit = null;

        private final function __construct() {
            	
        }

        private final function __clone() {
            	
        }

        public static function resolveInstance() {
            if(self :: $unit === null) {
                self :: $unit = new Unit();
            }
            return self :: $unit;
        }

        public function __call($method, $arguments) {
            return null;
        }

        public function __set($property, $value) {

        }

        public function __get($property) {
            return null;
        }

        public function __isset($property) {
            return false;
        }

        public function __unset($property) {
        }

        public function __toString() {
            return 'unit';
        }
    }
}

namespace {


    // This is a more general php-style abstraction of $array[$key]
    function array_get($array, $key) {

        if(isset($array[$key])) {
            return $array[$key];
        }

        return null;
    }

    // Union array $to into array $from
    function array_union_recursive(array &$from, array $to) {
        foreach($to as $key => $value) {
            if(isset($from[$key]) && is_array($from[$key]) && is_array($value)) {
                array_union_recursive($from[$key], $value);
                continue;
            }
            $from[$key] = $value;
        }
    }

    /**
     * This function returns the first element in a traversable object or null if no such element exists.
     * If the first element is null the function will return null as well, in order to prevent ambiguous results
     * the function should be called with a traversable which does contain null values.
     *
     * @param array|\IteratorAggregate|\Iterator $arg
     *
     * @return mixed|null The first element in the traversable object, or null if no such element exists
     */
    function first($arg) {
        if(is_array($arg)) {
            return count($arg) > 0 ? reset($args) : null;
        } elseif($arg instanceof Iterator) {
            $arg->rewind();
            return $arg->valid() ? $arg->current() : null;
        } elseif($arg instanceof IteratorAggregate) {
            return first($arg->getIterator());
        }

        return null;
    }

    /**
     * Recursively create an ugo+rwx chain of
     * directories. Return true if the given
     * directory exists.
     *
     * @param $pathname
     */
    function mkdirrwx($pathname) {
        if(file_exists($pathname)) {
            return true;
        }

        $umask = umask(0);
        $result = @mkdir($pathname, 0777, true);
        umask($umask);

        return $result;
    }
}