<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage core
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

namespace mg;

/**
 * An mgAssertionException is thrown when {@link mgCore :: getAssertEnabled()} yields and an assertion has failed.
 *
 * @author Michiel Hakvoort
 * @version 1.0
 * @see mgCore
 */
class AssertionException extends Exception {

    private $assertion;

    /**
     * Construct a new mgAssertionException.
     *
     *
     * @access public
     * @param string $message The message which the mgAssertionException carries
     * @param string $assertion The assertion that has failed
     * @param string $file The file in which the assertion has failed
     * @param int $line The line in the file where the assertion has failed
     */
    public function __construct($message, $assertion, $file, $line) {
        parent :: __construct($message);
        $this->assertion = $assertion;
        $this->file = $file;
        $this->line = $line;
    }

    /**
     * The assertion failure handler which will throw an exception
     *
     * @static
     * @access public
     * @param int $file
     * @param string $line
     * @param string $assertion
     */
    public static function assertionHandler($file, $line, $assertion) {
        $message = "Assertion ";
        $message .= mb_strlen($assertion) > 0 ? "'$assertion' " : "";
        $message .= "failed";
        throw new AssertionException($message, $assertion, $file, $line);
    }

    /**
     * Return the failed assertion
     *
     * @return string
     */
    public function getAssertion() {
        return $this->assertion;
    }
}

