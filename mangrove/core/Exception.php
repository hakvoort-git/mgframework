<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage core
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

namespace mg;

/**
 * The mg\Exception class is a normal Exception with the added possibility to get the source code causing the exception.
 *
 * @author Michiel Hakvoort
 * @version 1.0
 */
class Exception extends \Exception {

    /**
     * Get a context based error message from where the exception was thrown
     *
     * @param int $beforeContext The amount of lines before the exception line.
     * @param int $afterContext The amount of lines after the exception line.
     * @return array The contextual source lines.
     */
    public function getLineCode($beforeContext = 2, $afterContext = 2) {
        if(!(isset($this->file) && file_exists($this->file))) {
            return array();
        }

        $beforeContext = $this->line - max($beforeContext, 0);
        $afterContext = $this->line + max($afterContext, 0);

        $fp = fopen($this->file, 'r');
        $lineNumber = 0;
        $lines = array();
        while($lineNumber <= $afterContext && !feof($fp)) {
            $lineNumber++;
            $line = fgets($fp);
            if($lineNumber >= $beforeContext && $lineNumber <= $afterContext) {
                $lines[$lineNumber] = rtrim($line);
            }
        }
        fclose($fp);

        return $lines;
    }
}

/**
 * The mgClassNotFoundException is thrown when a classload is attempted,
 * but the class is not found.
 *
 * @author Michiel Hakvoort
 * @version 1.0
 * @see mgClassLoader
 */
class ClassNotFoundException extends Exception {

    public function __construct($message) {
        parent :: __construct($message);
    }

}

/**
 * The mgExceptionsInInitializerException is thrown when during a static class
 * method call during initialization an exception was thrown.
 *
 * @author Michiel Hakvoort
 * @version 1.0
 * @see mgClassLoader
 */
class ExceptionsInInitializerException extends Exception {

    private $exceptions;

    public function __construct($message, array $exceptions) {
        parent :: __construct($message);
        $this->exceptions = $exceptions;
    }

    /**
     * Return the {@link Exception exceptions} which caused the initialization failure.
     *
     * @return array An array of Exceptions caught during initialization
     */
    public function getExceptions() {
        return $this->exceptions;
    }
}