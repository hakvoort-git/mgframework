<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage core
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

namespace mg;

class XCache extends AbstractCache {


    public function __construct() {
        // extension_loaded('xcache')
    }


    public function get($key) {
        return xcache_get($key);
    }

    public function put($key, $value, $ttl = 0) {
        return xcache_set($key, $value, $ttl);
    }

    public function remove($key) {
        return xcache_unset($key);
    }

    public function contains($key) {
        return xcache_isset($key);
    }

    public function clear() {
        xcache_clear_cache(XC_TYPE_VAR);
    }

    public function getOffsets() {
        throw new BadMethodCallException('XCache :: getOffsets() not implemented');
    }

    public function current() {
        throw new BadMethodCallException('XCache :: current() not implemented');
    }

    public function next() {
        throw new BadMethodCallException('XCache :: next() not implemented');
    }

    public function key() {
        throw new BadMethodCallException('XCache :: key() not implemented');
    }

    public function valid() {
        throw new BadMethodCallException('XCache :: valid() not implemented');
    }

    public function rewind() {
        throw new BadMethodCallException('XCache :: rewind() not implemented');
    }
}
