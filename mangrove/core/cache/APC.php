<?php

/**
 * @copyright Michiel Hakvoort 2010
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD
 * @package mangrove
 * @subpackage core
 * @filesource
 */

/*
 * Copyright (c) 2010 Michiel Hakvoort
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

namespace mg;

use BadMethodCallException;

class APC extends AbstractCache {

    public function __construct() {
        /*		if(mgCore :: getMode() & mgCore :: PRODUCTION) {
         ini_set('apc.stat', 0);
         }
         */
    }

    public function get($key) {
        return apc_fetch($key);
    }

    public function put($key, $value, $ttl = 0) {
        return apc_store($key, $value, $ttl);
    }

    public function remove($key) {
        return apc_delete($key);
    }

    public function contains($key) {
        return apc_fetch($key) !== false;;
    }

    public function clear() {
        apc_clear_cache('user');
    }

    public function getOffsets() {
        throw new BadMethodCallException('APC :: getOffsets() not implemented');
    }

    public function current() {
        throw new BadMethodCallException('APC :: current() not implemented');
    }

    public function next() {
        throw new BadMethodCallException('APC :: next() not implemented');
    }

    public function key() {
        throw new BadMethodCallException('APC :: key() not implemented');
    }

    public function valid() {
        throw new BadMethodCallException('APC :: valid() not implemented');
    }

    public function rewind() {
        throw new BadMethodCallException('APC :: rewind() not implemented');
    }
}
